<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/styleetud.css">
	
	<title>Espace Etudiant</title>
	<meta charset="utf-8">
</head>
<body>
	<div class="container">
		<!--menu d'information des etudiants-->
		<div class="container1">
			<h3>information</h3>
			<img src="img/information.png">
			<p>
				<span class="info">Nom: </span><span class="rep"><c:out value="${nom}"></c:out></span><br><br><br>
				<span class="info">Prenom: </span><span class="rep"><c:out value="${prenom}"></c:out></span><br><br><br>
				<span class="info">CIN: </span><span class="rep"><c:out value="${CIN}"></c:out></span><br><br><br>
				<span class="info">Type de chambre: </span><span class="rep"><c:if test="${type_chambre!=null }"><c:out value="${type_chambre}"></c:out></c:if><c:if test="${type_chambre==null }"><c:out value="aucune "></c:out></c:if></span><br><br><br>
				<span class="info">numéro de chambre: </span><span class="rep"><c:if test="${numero_chambre!=0 }"><c:out value="${numero_chambre}"></c:out></c:if><c:if test="${etudiant.numero_chambre==0 }"><c:out value="aucune "></c:out></c:if></span><br><br>
				<span class="info">Batiment: </span><span class="rep"><c:if test="${battiement!=null }"><c:out value="${battiement}"></c:out></c:if><c:if test="${battiement==null }"><c:out value="aucun "></c:out></c:if></span><br>

			</p>
		</div>
		<div class="container2">

			<ul>
				<li id="Acceuil"><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li id="Catalogue"><a href="acceuil#cata" id="Catalogue">Catalogue</a></li>
				<li  id="Deconnexion"><a href="Desconnexion"  id="Deconnexion">Deconnexion</a></li>
			</ul>
		</div>

		<h1>Bienvenu dans votre espace etudiant </h1>
		<div class="menu">
			<div class="depot">
				<img src="img/deposer.png">
				<h2>Depot des fichier</h2>
				<p>Ce menu vous permet de deposer les piéces d'inscription dans l'internat</p>
				<a href="Depot">deposer</a>
			</div>
			<div class="reservation">
				<img src="img/reserver.png">
				<h2>reserveation de chambre</h2>
				<p>Ce menu vous permet de reserver votre future chambre</p>
				<a href="Reservation">reserver</a>
			</div>
			<div class="payement">
				<img src="img/payer.png">
				<h2>Payement</h2>
				<p>Ce menu vous permet de payer les frais d'inscription</p>
				<a href="Payement">payer</a>
			</div>
			<div class="reclamation">
				<img src="img/recla.png">
				<h2>Reclamation</h2>
				<p>Ce menu vous permet d'effectuer des reclamations concernant votre chambre,cuisine,toilettes</p>
				<a href="Reclamation">reclamer</a>
			</div>
		</div>
          <img src="afficher?CIN=<c:out value="${CIN}"></c:out>"  id="user">
          
	</div>

</body>
</html>