<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/stylegfichier.css">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<title>gestionfichiers</title>
</head>
<body>
<div class="container">
	<div class="barre">
		<ul>
			
				<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="MenuAdmin" id="espace">Mon espace</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Deconnexion</a></li>

		</ul>
	</div>
	<div class="image"><h1>Menu gestion des fichiers</h1></div>
	
	<div class="container1">
		<img src="img/listf.jpg">
		<h2>consultation fichiers</h2>
		<br>
		<div class="fvalid">
			<img src="img/fichierv.jpg">
			<h2>liste des fichiers valides</h2>
			<br><br>
			<table>
				<tr>
					<th>CIN_etudiant</th>
					<th>id_fichier</th>
					<th>type</th>
					<th>etat</th>
					<th>fichier</th>
				</tr>
				<c:forEach items="${liste1}" var="ligne">
				<tr>
				
				<td><c:out value="${ligne.CIN }"></c:out></td>
				<td><c:out value="${ligne.id_fichier }"></c:out></td>
				<td><c:out value="${ligne.type }"></c:out></td> 
				<td><c:out value="${ligne.etat }"></c:out></td>
				<td><a href="AfficherPieces?id_fichier=<c:out value="${ligne.id_fichier}"></c:out>&type=<c:out value="${ligne.type}"></c:out>" target="blank">visualiser</a></td>
				<tr>
			</c:forEach>
			
			</table>
		</div><br><br>
		<div class="fattente">
			<img src="img/fichera.jpg">
			<h2>liste des fichiers en attentes</h2>
			<br><br>
			<table>
				<tr>
					<th>CIN_etudiant</th>
					<th>id_fichier</th>
					<th>type</th>
					<th>etat</th>
					<th>fichier</th>
				</tr>
				<c:forEach items="${liste2}" var="ligne">
				<tr>
				<td><c:out value="${ligne.CIN }"></c:out></td>
				<td><c:out value="${ligne.id_fichier }"></c:out></td>
				<td><c:out value="${ligne.type }"></c:out></td> 
				<td><c:out value="${ligne.etat }"></c:out></td>
				<td><a href="AfficherPieces?id_fichier=<c:out value="${ligne.id_fichier}"></c:out>&type=<c:out value="${ligne.type}"></c:out>" target="blank">visualiser</a></td>
				<tr>
			</c:forEach>
			</table>
		</div>
		<br><br>
		<div class="fnvalide">
			<img src="img/fichiern.png">
			<h2>liste des fichiers non valides</h2>
			<br><br>
			<table>
				<tr>
					<th>CIN_etudiant</th>
					<th>id_fichier</th>
					<th>type</th>
					<th>etat</th>
					<th>fichier</th>
				</tr>
				<c:forEach items="${liste3}" var="ligne">
				<tr>
				<td><c:out value="${ligne.CIN }"></c:out></td>
				<td><c:out value="${ligne.id_fichier }"></c:out></td>
				<td><c:out value="${ligne.type }"></c:out></td> 
				<td><c:out value="${ligne.etat }"></c:out></td>
				<td><a href="AfficherPieces?id_fichier=<c:out value="${ligne.id_fichier}"></c:out>&type=<c:out value="${ligne.type}" ></c:out>" target="blank">visualiser</a></td>
				
				<tr>
			</c:forEach>
			</table>
		</div>
		
	</div>
	
	<div class="container2">
		<div class="ajout">
			<form method="post" id="ajouter" action="gfichier" >
				<img src="img/fm.png">
				<h3>Traitement fichier</h3>
				<h5><c:out value="${ resultat}"></c:out></h5>
				<label for="id_fichier">saisiser id du fichier</label><br>
				<input type="text" name="id_fichier" required="required" id="id_fichier"><input type="submit" name="chercher" value="chercher" class="button">
				<br><label>type fichier</label><span><c:if test="${Fichier.type==null and !empty Fichier}"><c:out value="Aucun fichier  trouvé"></c:out></c:if>
				<c:if test="${Fichier.type !=null }"><c:out value="${Fichier.type }"></c:out></c:if></span>
				<br><label>etat</label><span><c:if test="${Fichier.etat==null and !empty Fichier}"><c:out value="Aucun fichier  trouvé"></c:out></c:if>
				<c:if test="${Fichier.etat !=null }"><c:out value="${Fichier.etat }"></c:out></c:if></span><br>
				<label>CIN étudiant</label><span><c:if test="${Fichier.CIN==null and !empty Fichier}"><c:out value="etudiant supprimé"></c:out></c:if>
				<c:if test="${Fichier.CIN !=null }"><c:out value="${Fichier.CIN }"></c:out></c:if></span><br>
				<a href="AfficherPieces?id_fichier=<c:out value="${Fichier.id_fichier}"></c:out>&type=<c:out value="${Fichier.type}" ></c:out>" target="blank">visualiser</a><br><br>
				<input type="submit" name="valider" value="valider" >
				<input type="submit" name="nvalider" value="invalider">
				
				
			</form>
		</div>
		
</div>
</body>
</html>