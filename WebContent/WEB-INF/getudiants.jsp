<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/stylegetud.css">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<title>gestionetudiants</title>
</head>
<body>
<div class="container">
	<div class="barre">
		<ul>
				<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="MenuAdmin" id="espace">Mon espace</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Deconnexion</a></li>

		</ul>
	</div>
	<div class="image"><h1>Menu gestion des étudiants</h1></div>
	
	<div class="container1">
		<img src="img/list1.png">
		<h2>liste des étudiant inscrits</h2>
		<table>
			<tr>
				<th>Nom</th>
				<th>Prenom</th>
				<th>CIN</th>
				<th>Sexe</th>
				<th>Numéro chambre</th>
				<th>battiement</th>
			</tr>
			<c:forEach items="${liste}" var="ligne">
			<tr>
				<td><c:out value="${ligne.nom }"></c:out></td>
				<td><c:out value="${ligne.prenom }"></c:out></td> 
				<td><c:out value="${ligne.CIN }"></c:out></td>
				<td><c:out value="${ligne.sexe }"></c:out></td>
				<td><c:if test="${ligne.numero_chambre==0 and !empty ligne}"><c:out value="Aucune chambre"></c:out></c:if>
				<c:if test="${ligne.numero_chambre !=0 }"><c:out value="${ligne.numero_chambre }"></c:out></c:if></td>
				<td><c:if test="${ligne.battiement==null and !empty ligne}"><c:out value="Aucune chambre"></c:out></c:if>
				<c:if test="${ligne.battiement !=null }"><c:out value="${ligne.battiement }"></c:out></c:if></td>
				
			<tr>
			</c:forEach>
		</table>
	</div>
	
	<div class="container2">
		<div class="ajout">
			<form method="post" id="ajouter" action="getudiant" >
				<img src="img/ajout.png">
				<h3>Ajouter etudiant</h3>
				<label for="nom">Nom</label><br>
				<input type="text" name="nom" required="required" id="nom"><br>
				<label for="prenom">prenom</label><br>
				<input type="text" name="prenom" required="required" id="prenom"><br>
				<label for="cin">CIN</label><br>
				<input type="text" name="CIN" required="required" id="cin"><br>
				<input type="submit" name="ajouter" value="ajouter" >
				<h5><c:out value="${ resultat1}"></c:out></h5>
			</form>
		</div>
		<div class="supression">
			
			<form method="post" id="supprimer2" action="getudiant">
				<img src="img/supprime.png">
				<h3>Supprimer étudiant</h3>
				<label for="CIN">CIN de l'étudiant à supprimer</label>
				<input type="text" name="CIN" required="required" id="CIN">
				<input type="submit" name="chercher" value="chercher" >
				<br><label>nom</label><span><c:if test="${etudiant.prenom==null and !empty etudiant}"><c:out value="Aucun etudiant trouvé"></c:out></c:if>
				<c:if test="${etudiant.nom !=null }"><c:out value="${etudiant.nom }"></c:out></c:if></span><br>
				<br><label>prenom</label><span><c:if test="${etudiant.prenom==null and !empty etudiant}"><c:out value="Aucun etudiant trouvé"></c:out></c:if>
				<c:if test="${etudiant.prenom !=null }"><c:out value="${etudiant.prenom }"></c:out></c:if></span><br>
				<br><label>sexe</label><span><c:out value="${etudiant.sexe }"></c:out></span><br>
				<input type="submit" name="supprimer" value="supprimer">
				<h5><c:out value="${ resultat}"></c:out></h5>
			</form>
		</div>
	</div>
</div>
</body>
</html>