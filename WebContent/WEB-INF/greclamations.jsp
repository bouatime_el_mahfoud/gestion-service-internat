<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/stylereclamation.css">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<title>gestionreclamation</title>
</head>
<body>
<div class="container">
	<div class="barre">
		<ul>
				<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="MenuAdmin" id="espace">Mon espace</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Déconnexion</a></li>

		</ul>
	</div>
	<div class="image"><h1>Menu gestion des reclamations</h1></div>
	
	<div class="container1">
		<img src="img/lr.png">
		<h2>consultation reclamations</h2>
		<br>
		<div class="fvalid">
			<img src="img/rv.png">
			<h2>liste des reclamations valides</h2>
			<br><br>
			<table>
				<tr>
					<th>id_reclamation</th>
					<th>Endroit</th>
					<th>CIN_etudiant</th>
					<th>etat</th>
					
				</tr>
			<c:forEach items="${liste1}" var="ligne">
				<tr>
				<td><c:out value="${ligne.id_reclamation }"></c:out></td>
				<td><c:out value="${ligne.type }"></c:out></td> 
				<td><c:out value="${ligne.cin }"></c:out></td>
				<td><c:out value="${ligne.etat }"></c:out></td>
				
				<tr>
			</c:forEach>
			</table>
		</div><br><br>
		<div class="fattente">
			<img src="img/rea.png">
			<h2>liste des reclamations en attentes</h2>
			<br><br>
			<table>
				<tr>
					<th>id_reclamation</th>
					<th>Endroit</th>
					<th>CIN_etudiant</th>
					<th>etat</th>
				</tr>
			<c:forEach items="${liste2}" var="ligne">
				<tr>
				<td><c:out value="${ligne.id_reclamation }"></c:out></td>
				<td><c:out value="${ligne.type }"></c:out></td> 
				<td><c:out value="${ligne.cin }"></c:out></td>
				<td><c:out value="${ligne.etat }"></c:out></td>
				
				<tr>
			</c:forEach>
			</table>
		</div>
		<br><br>
		<div class="fnvalide">
			<img src="img/ri.png">
			<h2>liste des reclamations non valides</h2>
			<br><br>
			<table>
				<tr>
					<th>id_reclamation</th>
					<th>Endroit</th>
					<th>CIN_etudiant</th>
					<th>etat</th>
				</tr>
				<c:forEach items="${liste3}" var="ligne">
			<tr>
				<td><c:out value="${ligne.id_reclamation }"></c:out></td>
				<td><c:out value="${ligne.type }"></c:out></td> 
				<td><c:out value="${ligne.cin }"></c:out></td>
				<td><c:out value="${ligne.etat }"></c:out></td>
				
			<tr>
			</c:forEach>
			</table>
		</div>
		
	</div>
	
	<div class="container2">
		<div class="ajout">
			<form method="post" id="ajouter" action="gestionreclamation" >
				<img src="img/tr1.png">
				<h3>Traitement reclamations</h3>
				<label for="id_reclamation">saisissez id_reservation</label><br>
				<input type="number" name="id_reclamation" required="required" id="id_reclamation"><input type="submit" name="chercher" value="chercher" >
				<br><label>id_reclamation</label><span><c:if test="${reclamation.id_reclamation==0 }"><c:out value="Aucune reclamation trouvé"></c:out></c:if>
				<c:if test="${reclamation.id_reclamation !=0 }"><c:out value="${reclamation.id_reclamation}"></c:out></c:if></span>
				<br><label>etat:</label><span><c:if test="${reclamation.etat==null and !empty reclamation}"><c:out value="Aucune reclamation  trouvé"></c:out></c:if>
				<c:if test="${reclamation.etat !=null }"><c:out value="${reclamation.etat }"></c:out></c:if></span><br>
				<label>CIN étudiant:</label><span><c:if test="${reclamation.cin==null and !empty reclamation}"><c:out value="Aucun etudiant trouvé"></c:out></c:if>
				<c:if test="${reclamation.cin !=null }"><c:out value="${reclamation.cin }"></c:out></c:if></span><br><br>
				<label>Endroit:</label><span><c:if test="${reclamation.type==null and !empty reclamation}"><c:out value="Aucune reclamation trouvé"></c:out></c:if>
				<c:if test="${reclamation.type !=null }"><c:out value="${reclamation.type }"></c:out></c:if></span><br><br>
				<label>Description</label><br>
				<p style="color:red;"> <c:if test="${reclamation.description==null and !empty reclamation}"><c:out value="Aucun reclamation trouvé"></c:out></c:if>
				<c:if test="${reclamation.description !=null }"><c:out value="${reclamation.description }"></c:out></c:if></p><br><br>
				
				<input type="submit" name="valider" value="valider" >
				<input type="submit" name="nvalider" value="invalider"><br>
				<h5><c:out value="${ resultat}"></c:out></h5>
			</form>
		</div>
		
</div>
</body>
</html>