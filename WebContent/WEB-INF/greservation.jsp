<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/stylegreservation.css">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<title>gestionreservation</title>
</head>
<body>
<div class="container">
	<div class="barre">
		<ul>
				<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="MenuAdmin" id="espace">Mon espace</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Deconnexion</a></li>

		</ul>
	</div>
	<div class="image"><h1>Menu gestion des reservations</h1></div>
	
	<div class="container1">
		<img src="img/lr.png">
		<h2>consultation reservations</h2>
		<br>
		<div class="fvalid">
			<img src="img/rv.png">
			<h2>liste des reservations valides</h2>
			<br><br>
			<table>
				<tr>
					<th>id_reservation</th>
					<th>id_chambre</th>
					<th>CIN_etudiant</th>
					<th>etat</th>
					
				</tr>
				<c:forEach items="${liste1}" var="ligne">
			<tr>
				<td><c:out value="${ligne.id_reservation }"></c:out></td>
				<td><c:out value="${ligne.id_chambre }"></c:out></td> 
				<td><c:out value="${ligne.CIN }"></c:out></td>
				<td><c:out value="${ligne.etat }"></c:out></td>
				
			<tr>
			</c:forEach>
			</table>
		</div><br><br>
		<div class="fattente">
			<img src="img/rea.png">
			<h2>liste des reservations en attentes</h2>
			<br><br>
			<table>
				<tr>
					<th>id_reservation</th>
					<th>id_chambre</th>
					<th>CIN_etudiant</th>
					<th>etat</th>
				</tr>
				<c:forEach items="${liste2}" var="ligne">
			<tr>
				<td><c:out value="${ligne.id_reservation }"></c:out></td>
				<td><c:out value="${ligne.id_chambre }"></c:out></td> 
				<td><c:out value="${ligne.CIN }"></c:out></td>
				<td><c:out value="${ligne.etat }"></c:out></td>
				
			<tr>
			</c:forEach>
			</table>
		</div>
		<br><br>
		<div class="fnvalide">
			<img src="img/ri.png">
			<h2>liste des reservations non valides</h2>
			<br><br>
			<table>
				<tr>
					<th>id_reservation</th>
					<th>id_chambre</th>
					<th>CIN_etudiant</th>
					<th>etat</th>
				</tr>
				<c:forEach items="${liste3}" var="ligne">
			<tr>
				<td><c:out value="${ligne.id_reservation }"></c:out></td>
				<td><c:out value="${ligne.id_chambre }"></c:out></td> 
				<td><c:out value="${ligne.CIN }"></c:out></td>
				<td><c:out value="${ligne.etat }"></c:out></td>
				
			<tr>
			</c:forEach>
			</table>
		</div>
		
	</div>
	
	<div class="container2">
		<div class="ajout">
			<form method="post" id="ajouter" action="greservations" >
				<img src="img/tr.png">
				<h3>Traitement reservation</h3>
				<label for="id_reservation">saisissez id_reservation</label><br>
				<input type="number" name="id_reservation" required="required" id="id_reservation"><input type="submit" name="chercher" value="chercher" >
				<br><label>id_reservation</label><span><c:if test="${reservation.id_reservation==0 }"><c:out value="Aucune reservation trouvé"></c:out></c:if>
				<c:if test="${reservation.id_reservation !=0 }"><c:out value="${reservation.id_reservation}"></c:out></c:if></span>
				<br><label>etat</label><span><c:if test="${reservation.etat==null and !empty reservation}"><c:out value="Aucune reservation  trouvé"></c:out></c:if>
				<c:if test="${reservation.etat !=null }"><c:out value="${reservation.etat }"></c:out></c:if></span><br>
				<label>CIN étudiant</label><span><c:if test="${reservation.CIN==null and !empty reservation}"><c:out value="Aucun etudiant trouvé"></c:out></c:if>
				<c:if test="${reservation.CIN !=null }"><c:out value="${reservation.CIN }"></c:out></c:if></span><br><br>
				
				<input type="submit" name="valider" value="valider" >
				<input type="submit" name="nvalider" value="invalidé">
				<h5><c:out value="${ resultat}"></c:out></h5>
			</form>
		</div>
		
</div>
</body>
</html>