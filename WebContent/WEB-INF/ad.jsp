<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>administrateur</title>
	<link rel="stylesheet" type="text/css" href="css/adc.css">
	<meta charset="utf-8">
</head>
<body>
	<div class="container">
		<!--la barre de navigation-->
		<div class="barre">
			<ul>
				<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Deconnexion</a></li>
			</ul>
		</div>
		<!--fin de la barre de navigation-->
		<h1>Bienvenu dans l'espace Administrateur</h1>
		<div class="menu">
			<div class="gestion-etudiants">
				<img src="img/etudiant.jpg">
				<h2>gestion des étudiants</h2>
				<p>Ce menu vous permet la gestion des étudiants inscrits dans votre service internat </p>
				<a href="getudiant">accéder</a>
			</div>
			<div class="gestion-fichiers">
				<img src="img/gf2.png">
				<h2>gestion des fichiers</h2>
				<p>Ce menu vous permet de verifier la  validiter des piéces d'inscriptions  </p>
				<a href="gfichier">accéder</a>
			</div>
			<div class="gestion-reservation">
				<img src="img/gr.png">
				<h2>gestion des reservations</h2>
				<p>Ce menu vous permet de valider ou d'annuler une reservation d'un étudiant</p>
				<a href="greservations">accéder</a>
			</div>
			<div class="payement">
				<img src="img/gp2.jpg">
				<h2>gestion des payements</h2>
				<p>Ce menu vous permet de gérer les payements effectué par les étudiants</p>
				<a href="gpayement">acceder</a>
			</div>
			<div class="reclamation">
				<img src="img/gr1.jpg">
				<h2>gestion des réclamations</h2>
				<p>Ce menu vous permet la gestion des réclamations effectué par les étudiants</p>
				<a href="gestionreclamation">acceder</a>
			</div>
			<div class="gestion-chambre">
				<img src="img/chambre.jpg">
				<h2>gestion des chambres</h2>
				<p>Ce menu vous permet la gestion des chambres  dans votre service internat  </p>
				<a href="gchambre">accéder</a>
			</div>
		</div>
		<div class="chambre">
			<h2>recherche d'une chambre</h2>
			<form method="post" action="MenuAdmin">
				<label for="numero_chambre">Numéro de chambre</label>
				<input type="number" name="numero_chambre" id="numero_chambre"  required="required" autofocus="autofocus"><br><br>
				<label for="battiement"> Nom du batiment</label>
				<input type="text" name="battiement" id="battiement"  required="required"><input type="submit" name="cherchec" value="chercher">
			</form>
			<p>
				<span class="info">Numero de la chambre:</span><span class="rep"><c:if test="${chambre.n_chambre==0 }"><c:out value="Aucune chambre trouvé"></c:out></c:if>
				<c:if test="${chambre.n_chambre !=0 }"><c:out value="${chambre.n_chambre}"></c:out></c:if></span><br><br>
				<span class="info">Batiment:</span><span class="rep"><c:if test="${chambre.battiement==null and !empty chambre}"><c:out value="Aucune chambre trouvé"></c:out></c:if>
				<c:if test="${chambre.battiement !=null }"><c:out value="${chambre.battiement }"></c:out></c:if></span><br><br>
				<span class="info">Type de chambre:</span><span class="rep"><c:if test="${chambre.type==null and !empty chambre}"><c:out value="Aucune chambre trouvé"></c:out></c:if>
				<c:if test="${chambre.type !=null }"><c:out value="${chambre.type }"></c:out></c:if></span><br><br>
				<span class="info">état de la chambre</span><span class="rep"><c:if test="${chambre.etat==null and !empty chambre}"><c:out value="Aucune chambre trouvé"></c:out></c:if>
				<c:if test="${chambre.etat !=null }"><c:out value="${chambre.etat }"></c:out></c:if></span>
			</p>
			
		</div>
		<div class="etudiant">
			<h2>Recherche d'un étudiant</h2>
			<form action="MenuAdmin" method="post">
				<label for="cin">CIN</label>
				<input type="text" name="CIN" id="CIN"  required="required" autofocus="autofocus">
				<input type="submit" name="cherchere" value="chercher">
				
			<p>
				<br><span class="info1">CIN:</span><span class="rep1"><c:if test="${etudiant.CIN==null and !empty etudiant}"><c:out value="Aucun etudiant trouvé"></c:out></c:if>
				<c:if test="${etudiant.CIN !=null }"><c:out value="${etudiant.CIN }"></c:out></c:if></span><br><br>
				<span class="info">Prenom:</span><span class="rep1"><c:if test="${etudiant.prenom==null and !empty etudiant}"><c:out value="Aucun etudiant trouvé"></c:out></c:if>
				<c:if test="${etudiant.prenom !=null }"><c:out value="${etudiant.prenom }"></c:out></c:if></span><br><br>
				<span class="info">Nom:</span><span class="rep"><c:if test="${etudiant.prenom==null and !empty etudiant}"><c:out value="Aucun etudiant trouvé"></c:out></c:if>
				<c:if test="${etudiant.nom !=null }"><c:out value="${etudiant.nom }"></c:out></c:if></span><br><br>
				<span class="info">Numéro de la chambre:</span><span class="rep1"><c:if test="${etudiant.numero_chambre==0 and !empty etudiant }"><c:out value="Aucune chambre affecté"></c:out></c:if><c:if test="${etudiant.numero_chambre!=0 }"><c:out value="${etudiant.numero_chambre }"></c:out></c:if></span><br><br>
				<span class="info">Nom du batiment:</span><span class="rep1"><c:if test="${etudiant.battiement==null and !empty etudiant }"><c:out value="Aucune chambre affecté"></c:out></c:if><c:if test="${etudiant.numero_chambre!=null}"><c:out value="${etudiant.battiement }"></c:out></c:if></span>
			</p>
			</form>
		</div>
	</div>
</body>
</html>