
<!DOCTYPE html>
<html >
    <head>
        <title> Inscription</title>
        <!-- Meta tag Keywords -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <meta name="keywords" content=" volunteer enrollment form Widget a Flat Responsive Widget,Login form widgets, Sign up Web forms , Login signup Responsive web form,Flat Pricing table,Flat Drop downs,Registration Forms,News letter Forms,Elements" />
        
        <!-- Meta tag Keywords -->
        <link rel="stylesheet" href="css1/styleinscription.css" type="text/css" media="all" /><!-- Style-CSS -->
        <link href="//fonts.googleapis.com/css?family=Heebo:300" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Exo" rel="stylesheet">
    </head>
    <body>
        <section class="agile-volt">
            <div class="agile-voltheader">
                <h1>Formulaire <label>d'inscription</label> </h1>
            </div>
            <div class="agile-voltsub">

                <form action="Inscription"  method="post" >
                    <div class="agile-name">
                        <p>Nom</p>
                        <input type="text" name="nom" placeholder="entrez votre nom" required="required">
                    </div>
                    <div class="clear"></div>
                    <div class="agile-email">
                        <p>Prenom</p>
                        <input type="text" name="prenom" placeholder="entrez votre prenom"  required="required">
                    </div>
                    <div class="clear"></div>
                    <div class="agile-address">
                        <p>CIN</p>
                        <input type="text" name="CIN" placeholder="entrez votre email"   required="required">
                    </div>
                     <div class="agile-address">
                        <p>Sexe</p>
                       <select name="sexe"  required="required">
                       		<option value="M">Masculin</option>
                       		<option value="F">Feminin</option>
                       </select>
                    </div>
                     <div class="agile-address">
                        <p>Email</p>
                        <input type="text" name="email" placeholder="entrez votre email"   required="required">
                    </div>
                    <div class="clear"></div>
                    <div class="agile-city">
                        <p>Mot de passe</p>
                        <input type="password" name="password" placeholder="mot de passe"  required="required" />
                    </div>
                    <div class="agile-city">
                     <p>Image</p>
                        <input type="file" name="image"  required="required">
					</div>
                    <div class="clear"></div>
       
                   
                    <div class="clear"></div>
                    <input type="submit" value="S'inscrire" />
                </form>
                </br>
                <div id="message">
                    <center style="color:#ff4500"><c:out value="${message}"></c:out></center> 
                </div>
            </div>

        </section>
    </body>
</html>
