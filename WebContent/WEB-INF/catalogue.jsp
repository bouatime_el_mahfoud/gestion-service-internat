
<!DOCTYPE html>
<html>
<head>
<title>Catalogue</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Consultancy Profile Widget Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- js -->
<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/sliding.form.js"></script>
<!-- //js -->
<link href="css1/stylep.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css1/font-awesome.min.css" />
<link rel="stylesheet" href="css1/smoothbox.css" type='text/css' media="all" />
<link href="//fonts.googleapis.com/css?family=Pathway+Gothic+One" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
    <body>
        <div class="main">
              <h1>Catalogue des chambres</h1>
              
              <div class="agile-voltsub">
                      <div class="work-w3agile">
							<div class="work-w3agile-top">
								 <div class="agileits_w3layouts_work_grid1 w3layouts_work_grid1 hover14 column">
									  <div class="w3_agile_work_effect">
										<ul>
											<li>
                                                                                            <p style="color:#FFFFFF">Chambre Simple </p>
                                                                                            </br>
												
													<figure>
														<img src="img/s.jpeg" alt=" " class="img-responsive" />
													</figure>
												</a>
											</li>
											<li>
                                                                                                <p style="color:#FFFFFF">Chambre double </p>
                                                                                                </br>
												
													<figure>
														<img src="img/d.jpg" alt=" " class="img-responsive" />
													</figure>
												</a>
											</li>
											<li>
                                                                                                <p style="color:#FFFFFF">Chambre triple</p>
                                                                                                </br>
												
													<figure>
														<img src="img/t.jpg" alt=" " class="img-responsive" />
													</figure>
												</a>
											</li>
                                                                                        </br>
                                                                                        </br>
                                                                                        
												<div class="clear"></div>
										</ul> 
									</div>
								</div>
							</div>
						</div>
                                                
            
            
         </div>
         </div>
    </body>
</html>
