<!--
	Author: W3layouts
	Author URL: http://w3layouts.com
	License: Creative Commons Attribution 3.0 Unported
	License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<!-- Head -->
<head>
<title>Projet</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta charset="utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="keywords" content="Solar Energy a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

<!--FlexSlider-->
	<link rel="stylesheet" href="css1/flexslider.css" type="text/css" media="screen" />
<!--//FlexSlider-->
	
<link href="css1/lsb.css" rel="stylesheet" type="text/css"><!-- gallery -->

<!-- custom css theme files -->
	<link rel="stylesheet" href="css1/bootstrap.min.css" type="text/css" media="all">
	<link rel="stylesheet" href="css1/font-awesome.min.css" />
	<link rel="stylesheet" href="css1/style1.css" type="text/css" media="all">
<!-- //custom css theme files -->

<!-- google fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'><!--web font-->
<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
<!-- //google fonts -->

</head>
<!-- Body -->
<body>
<!-- banner -->
<div class="w3l-banner">
<div class="wthree-dot">
		<!-- nav -->
		<div class="header w3layouts">
				<div class="container-fluid">
					<nav class="navbar navbar-default">
						<div class="navbar-header navbar-left">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
							<nav>
								<ul class="nav navbar-nav">
									<li class="active"><a href="acceuil">Acceuil</a></li>
									<li><a href="#about" class="scroll">A propos</a></li>
									<li class="active"><a href="Catalogue" >Catalogue</a></li>
									<li><a href="#services" class="scroll">Services</a></li>
									<li><a href="#contact" class="scroll">Contact</a></li>
                                    <li class="active"><a href="Inscription"> Inscription</a></li>
                                    <li class="active"><a href="Connexion"> Connexion</a></li>
								</ul>
								
							</nav>
						<ul class="top-links social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						</ul>
						</div>
					</nav>
				</div>
			</div>
		<!-- //nav -->

		<!-- //Header -->
		<div class="container">
			<div class="flexslider-info">
				<section class="slider">
					<div class="flexslider">
						
							 <div class="w3l-info">
								<h2>Bienvenue dans notre cite universitaire</h2>
								<p>Ce site va vous permettre d'effectuer l'inscription dans notre cite universitaire et de choisir ainsi les chambres qui vous plaisent et aussi d'effectuer les differents processus de paiement et  de reclamations .</p>
								</div>
							
							
							
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
<!-- //banner -->

<!-- banner bottom -->

<!-- //banner bottom -->

<!-- about -->
<div class="about" id="about">
	<div class="container">
		<h3 class="heading">A propos de nous</h3>
			<div class="abouttop">
				<div class="col-md-6" >
					<div class="abouttopleft" style="height: 360px">
						<h3>A propos de nos chambres</h3>
						<p><i class="fa fa-angle-right" aria-hidden="true"></i> Confort et satisfaction.</p>
						<p><i class="fa fa-angle-right" aria-hidden="true"></i> Prix abordables pour tout etudiant.</p>
						<p><i class="fa fa-angle-right" aria-hidden="true"></i> Diversit� de chambres.</p>
						
					</div>
				</div>
                            <div class="col-md-6" >
					<div class="abouttopright">
						<img src="images/about3.jpg" alt="" />
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="aboutbottom">
				<div class="col-md-6">
					<div class="aboutbottomleft">
						<img src="images/about5.jpg" alt="" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="aboutbottomright">
						<h3>A propos de nos conditions</h3>
	
						<p><i class="fa fa-angle-right" aria-hidden="true"></i> Vous devez repsecter le reglement de notre �cole.</p>
						<p><i class="fa fa-angle-right" aria-hidden="true"></i> Vous devez aussi fournir les diff�rents pieces d'inscriptions.</p>
						<p><i class="fa fa-angle-right" aria-hidden="true"></i> Vous pouvez a tout moment faire des reclamations s'il y'a un probleme dans nos locaux.</p>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
	</div>
</div>
<!-- //about -->



<!--/team-->

<!--/team-->

<!--gallery-->

	
<!--//gallery-->
		
<!--/reviews-->
</br></br></br>

<!--//reviews-->

<!-- blog -->
	
<!-- //blog -->
<!-- services -->
	<div class="services" id="services">
		<div class="container">
		<h3 class="heading">Services</h3>
			<div class="agileits-services">
				<div class="services-right-grids">
					<div class="col-sm-3 services-right-grid">
						<div class="services-icon hvr-radial-in">
							<i class="fa fa-thumbs-up" aria-hidden="true"></i>
						</div>
						<div class="services-icon-info">
							<h5> Responsabilite</h5>
							<p>la responsabilite de vous plaire nous pousse � innover et donner le meilleur de nous meme</p>
						</div>
					</div>
					<div class="col-sm-3 services-right-grid">
						<div class="services-icon hvr-radial-in">
							<i class="fa fa-refresh" aria-hidden="true"></i>
						</div>
						<div class="services-icon-info">
							<h5>Actualite</h5>
							<p>Envoyez nous des mails pour recevoir les nouveautes,les offres et les promotions durant l'annee.</p>
						</div>
					</div>
					<div class="col-sm-3 services-right-grid">
						<div class="services-icon hvr-radial-in">
							<i class="fa fa-check" aria-hidden="true"></i>
						</div>
						<div class="services-icon-info">
							<h5>Qualite</h5>
							<p>Nos produits vous assure une parfaite satisfaction,efficacite et autonomie</p>
						</div>
					</div>
					<div class="col-sm-3 services-right-grid">
						<div class="services-icon hvr-radial-in">
							<i class="fa fa-question" aria-hidden="true"></i>
						</div>
						<div class="services-icon-info">
							<h5>Questions</h5>
							<p>Donnez nous vos impressions en posant tous vos questions et vos commentaires.Nous serons ravis d'y repondre</p>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>
<!-- //services -->

<!-- contact -->
<div class="contact" id="contact">
	<div class="container">
		<h3 class="heading">Contactez nous</h3>
	</div>
	<div class="contactright1">
		<p><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>1680 Richmond St, Corner in, ON N58 3d7, China</p>
		<p><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>+0(12) 000 123 3120</p>
		<p><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:info@example.com">mail@example.com</a></p>
	</div>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26937440.454529278!2d86.06399232514998!3d34.49624725444937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31508e64e5c642c1%3A0x951daa7c349f366f!2sChina!5e0!3m2!1sen!2sin!4v1497505688868"></iframe>	
</div>	
<div class="contactform">
		<div class="container">
		<h3 class="heading">Ecrivez nous</h3>
			<div class="agileits_agile_about_mail">
				<form action="#" method="post">
					<div class="col-md-6 agileits_agile_about_mail_left">
						<input type="text" name="Name" placeholder="Nom" required="">
					<div class="clearfix"> </div>
					</div>
					<div class="col-md-6 agileits_agile_about_mail_left">
						<input type="text" name="Subject" placeholder="Sujet" required="">
					<div class="clearfix"> </div>
					</div>
					<div class="col-md-6 agileits_agile_about_mail_left">
						<input type="email" name="Email" placeholder="Email" required="">
					<div class="clearfix"> </div>
					</div>
					<div class="col-md-6 agileits_agile_about_mail_left">
						<input type="text" name="Phone" placeholder="Telephone"required="true" />
					<div class="clearfix"> </div>
					</div>
					<div class="clearfix"> </div>
					<textarea name="Message" placeholder="Message..." required=""></textarea>
					<div class="submit">
						<input type="submit" value="Submit">
					</div>
				</form>
			</div>
		</div>
</div>
<!-- //contact -->

<!-- copyright -->

<!-- //copyright -->

<!-- bootstrap-modal-pop-up -->
	
<!-- //bootstrap-modal-pop-up --> 

<!-- Default-JavaScript-File -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- //Default-JavaScript-File -->

<!--Start-slider-script-->
	<script defer src="js/jquery.flexslider.js"></script>
		<script type="text/javascript">
		
		$(window).load(function(){
		  $('.flexslider').flexslider({
			animation: "slide",
			start: function(slider){
			  $('body').removeClass('loading');
			}
		  });
		});
	  </script>
<!--End-slider-script-->

<!-- testimonials required-js-files-->
							<link href="css1/owl.carousel.css" rel="stylesheet">
							    <script src="js/owl.carousel.js"></script>
							        <script>
							    $(document).ready(function() {
							      $("#owl-demo").owlCarousel({
							        items : 1,
							        lazyLoad : true,
							        autoPlay : true,
							        navigation : false,
							        navigationText :  false,
							        pagination : true,
							      });
							    });
							    </script>
<!--// testimonials required-js-files-->

<!-- flexisel -->
		<script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo1").flexisel({
				visibleItems: 4,
				animationSpeed: 1000,
				autoPlay: true,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
				responsiveBreakpoints: { 
					portrait: { 
						changePoint:480,
						visibleItems: 1
					}, 
					landscape: { 
						changePoint:640,
						visibleItems:2
					},
					tablet: { 
						changePoint:768,
						visibleItems: 2
					}
				}
			});
			
		});
	</script>
	<script type="text/javascript" src="js/jquery.flexisel.js"></script>
<!-- //flexisel -->

<!-- gallery-pop-up -->
	<script src="js/lsb.min.js"></script>
	<script>
	$(window).load(function() {
		  $.fn.lightspeedBox();
		});
	</script>
<!-- //gallery-pop-up -->

<!-- smooth scrolling -->
	<script src="js/SmoothScroll.min.js"></script>
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
<!-- smooth scrolling -->

<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!-- //scrolling script -->
	
</body>
<!-- //Body -->
</html>