<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/gpayement.css">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<title>gestionpayement</title>
</head>
<body>
<div class="container">
	<div class="barre">
		<ul>
			<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="MenuAdmin" id="espace">Mon espace</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Déconnexion</a></li>

		</ul>
	</div>
	<div class="image"><h1>Menu gestion des payements</h1></div>
	
	<div class="container1">
		<img src="img/lp.png">
		<h2>consultation payements</h2>
		<br>
		<div class="fvalid">
			<img src="img/pv.png">
			<h2>liste des payements valides</h2>
			<br><br>
			<table>
				<tr>
					<th>id_payement</th>
					<th>mode</th>
					<th>CIN_étudiant</th>
					<th>etat</th>
					<th>tranche</th>
					<th>reçu</th>
				</tr>
				<c:forEach items="${liste1}" var="ligne">
				<tr>
				
				<td><c:out value="${ligne.id_payement }"></c:out></td>
				<td><c:out value="${ligne.mode }"></c:out></td>
				<td><c:out value="${ligne.CIN }"></c:out></td> 
				<td><c:out value="${ligne.etat }"></c:out></td>
				<td><c:out value="${ligne.tranche }"></c:out></td> 
				<td><a href="afficherp?id_payement=<c:out value="${ligne.id_payement}"></c:out>" target="blank">visualiser</a></td>
				<tr>
			</c:forEach>
			</table>
		</div><br><br>
		<div class="fattente">
			<img src="img/pa.png">
			<h2>liste des payements en attentes</h2>
			<br><br>
			<table>
				<tr>
					<th>id_payement</th>
					<th>mode</th>
					<th>CIN_étudiant</th>
					<th>etat</th>
					<td>tranche</td>
					<th>reçu</th>
				</tr>
				<c:forEach items="${liste2}" var="ligne">
				<tr>
				
				<td><c:out value="${ligne.id_payement }"></c:out></td>
				<td><c:out value="${ligne.mode }"></c:out></td>
				<td><c:out value="${ligne.CIN }"></c:out></td> 
				<td><c:out value="${ligne.etat }"></c:out></td>
				<td><c:out value="${ligne.tranche }"></c:out></td> 
				<td><a href="afficherp?id_payement=<c:out value="${ligne.id_payement}"></c:out>" target="blank">visualiser</a></td>
				<tr>
			</c:forEach>
			</table>
		</div>
		<br><br>
		<div class="fnvalide">
			<img src="img/ip1.png">
			<h2>liste des payements non valides</h2>
			<br><br>
			<table>
				<tr>
					<th>id_payement</th>
					<th>mode</th>
					<th>CIN_étudiant</th>
					<th>etat</th>
					<th>tranche</th>
					<th>reçu</th>
				</tr>
				<c:forEach items="${liste3}" var="ligne">
				<tr>
				<td><c:out value="${ligne.id_payement }"></c:out></td>
				<td><c:out value="${ligne.mode }"></c:out></td>
				<td><c:out value="${ligne.CIN }"></c:out></td> 
				<td><c:out value="${ligne.etat }"></c:out></td>
				<td><c:out value="${ligne.tranche }"></c:out></td> 
				<td><a href="afficherp?id_payement=<c:out value="${ligne.id_payement}"></c:out>" target="blank">visualiser</a></td>
				<tr>
			</c:forEach>
			</table>
		</div>
		
	</div>
	
	<div class="container2">
		<div class="ajout">
			<form method="post" id="ajouter" action="gpayement" >
				<img src="img/tp.png">
				<h3>Traitement payement</h3>
				
				<label for="id_payement">saisissez id du payement</label><br>
				<input type="text" name="id_payement" required="required" id="id_payement"><input type="submit" name="chercher" value="chercher" class="button">
				<br><label>mode</label><span><c:if test="${payement.mode==null and !empty payement}"><c:out value="Aucun payement  trouvé"></c:out></c:if>
				<c:if test="${payement.mode !=null }"><c:out value="${payement.mode }"></c:out></c:if></span>
				<br><label>etat</label><span><c:if test="${payement.etat==null and !empty payement}"><c:out value="Aucun payement  trouvé"></c:out></c:if>
				<c:if test="${payement.etat !=null }"><c:out value="${payement.etat }"></c:out></c:if></span><br>
				<label>CIN étudiant</label><span><c:if test="${payement.CIN==null and !empty payement}"><c:out value="Etudiant supprimé"></c:out></c:if>
				<c:if test="${payement.CIN !=null }"><c:out value="${payement.CIN }"></c:out></c:if></span><br>
				<label>tranche</label><span><c:if test="${payement.tranche==null and !empty payement}"><c:out value="Aucun payement  trouvé"></c:out></c:if>
				<c:if test="${payement.tranche !=null }"><c:out value="${payement.tranche }"></c:out></c:if></span><br>
				
				<a href="afficherp?id_payement=<c:out value="${payement.id_payement}"></c:out>" target="blank">visualiser</a>
				<input type="submit" name="valider" value="valider" >
				<input type="submit" name="nvalider" value="invalider"><h5><c:out value="${ resultat}"></c:out></h5>
				
			</form>
		</div>
		
</div>
</body>
</html>