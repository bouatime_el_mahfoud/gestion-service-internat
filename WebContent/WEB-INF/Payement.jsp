<!DOCTYPE html>
<html>
<head>
	<title>Menu de payement</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/stylepay.css">
	<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">
	 <script src="https://www.paypalobjects.com/api/checkout.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
</head>
<body>
<div class="container">
	<div class="barre">
		<ul>
				<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="Menuetud" id="espace">Mon espace</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Deconnexion</a></li>

		</ul>
	</div>
	<div class="container1">
		<div class="container2"><h1>Menu de Payement</h1></div>
		<p class="etat"><span>validit� du payement: </span><br><span id="fin"><c:out value="${message}"></c:out></span></p>
		<p id="info"><img src="img/information.png"><span id="h1"> informations</span><br>Bienvenu dans votre menu de payement ,ce menu a pour objectif de vous aider  � effectuer votre payement en ligne ,vous pouvez soit effectuez votre payement par votre carte bancaire � travers un service bancaire en ligne ou bien par le d�pot d'un fichier pdf qui contient le re�u du versement dans notre compte bancaire.</p>
		<form method="post" action="Payement">
			<img src="img/pay.png"><br>
			<h3>payement par d�pot du fichier</h3><br>
			<label >choisisez le payement par tranche ou la totalit�:</label><br><br><br>
			<label for="tranche" id="tranch" >tranche</label><input type="radio" name="tranche" id="tranche" value="tranche" required="required">
			<label for="totale" id="total">total</label><input type="radio" name="tranche" id="totale" value="totale"><br>
			<label for="fichier">votre recu sous format pdf</label><br><br><br>
			<input type="file" name="fichier" id="fichier" required="required"><br><br>
			
			<input type="submit" name="deposer" value="valider">
			<h5><c:out value="${message1 }"></c:out></h5>
		</form>
<div id="paypal-button"></div>

  <script>
    paypal.Button.render({
      env: 'sandbox', // Or 'sandbox',
       client: {
            sandbox:    'AUXaT5xS_nJQtxZnpAGbXY5jJyI2wpvFC-CPmSo2LA3IdBM2k_buEUFLN-r0MQGa-dB3h3XKfEDxN4c8',
            production: 'EAh43ZFexaBcaRmpN2tKgHfiLjdwsNWu3jT02yu0iiTe-n5ac1g8Kn7z-AUxdZd0Tq5Zzjcl8a_hOl5S'
        },


      commit: true, // Show a 'Pay Now' button

      style: {
        color: 'gold',
        size: 'small',

      },

      payment: function(data, actions) {
         return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: '250', currency: 'USD' }
                        }
                    ]
                }
            });
      },

      onAuthorize: function(data, actions) {
        return actions.payment.execute().then(function(payment) {
             window.alert('Payment Complete!');

                // The payment is complete!
                // You can now show a confirmation message to the customer
            });
      },

      onCancel: function(data, actions) {
        /* 
         * Buyer cancelled the payment 
         */
      },

      onError: function(err) {
        /* 
         * An error occurred during the transaction 
         */
      }
    }, '#paypal-button');
  </script>
		
		<p id="f2">imprimer votre re�u <a href="RecuPdf" target="blank">imprimer</a>	</p>	
		
	</div>
</div>
</body>
</html>