<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/stylechambre.css">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display" rel="stylesheet">
	<title>gestionetudiants</title>
</head>
<body>
<div class="container">
	<div class="barre">
		<ul>
				<li><a href="acceuil" id="logo"></a></li>
				<li><a href="acceuil" id="Acceuil">Acceuil</a></li>
				<li><a href="acceuil#catal" id="Catalogue">Catalogue</a></li>
				<li><a href="MenuAdmin" id="espace">Mon espace</a></li>
				<li><a href="Desconnexion" id="Deconnexion">Deconnexion</a></li>

		</ul>
	</div>
	<div class="image"><h1>Menu gestion des chambres</h1></div>
	
	<div class="container1">
		<img src="img/tc.png">
		<h2>liste des chambres </h2>
		<table>
			<tr>
				<th>id_chambre</th>
				<th>numero chambre</th>
				<th>etat</th>
				<th>type</th>
				<th>batiment</th>
			</tr>
			<c:forEach items="${liste}" var="ligne">
			<tr>
				<td><c:out value="${ligne.id_chambre }"></c:out></td>
				<td><c:out value="${ligne.n_chambre }"></c:out></td> 
				<td><c:out value="${ligne.etat }"></c:out></td>
				<td><c:out value="${ligne.type }"></c:out></td>
				<td><c:out value="${ligne.battiement }"></c:out></td>
			<tr>
			</c:forEach>
		</table>
	</div>
	
	<div class="container2">
		<div class="ajout">
			<form method="post" id="ajouter" >
				<img src="img/ac1.png">
				<h3>Ajouter chambre</h3>
				<label for="numero_chambre">numero_chambre</label><br>
				<input type="number" name="numero_chambre" required="required" id="numero_chambre"><br>
				<label for="type_chambre">type</label><br>
				<input type="text" name="type_chambre" required="required" id="type_chambre"><br>
				<label for="battiement">batiment</label><br>
				<input type="text" name="battiement" required="required" id="battiement"><br>
				<input type="submit" value="ajouter" name="ajouter" >
				<br><h5 id="h5"><c:out value="${ resultat1}"></c:out></h5>
			</form>
		</div>
		<div class="supression">
			
			<form method="post" id="supprimer2" action="gchambre">
				<img src="img/sc.png">
				<h3>Supprimer chambre</h3>
				<label for="id_chambre">id de la chambre à supprimer</label>
				<input type="text" name="id_chambre" required="required" id="id_chambre">
				<input type="submit" name="chercher" value="chercher" >
				<br><label>numero</label><span><c:if test="${chambre.n_chambre==0 }"><c:out value="Aucune chambre trouvé"></c:out></c:if>
				<c:if test="${chambre.n_chambre !=0 }"><c:out value="${chambre.n_chambre}"></c:out></c:if></span><br>
				<br><label>type</label><span><c:if test="${chambre.type==null and !empty chambre}"><c:out value="Aucune chambre trouvé"></c:out></c:if>
				<c:if test="${chambre.type !=null }"><c:out value="${chambre.type }"></c:out></c:if></span><br>
				<br><label>batiment</label><span><c:if test="${chambre.battiement==null and !empty chambre}"><c:out value="Aucune chambre trouvé"></c:out></c:if>
				<c:if test="${chambre.battiement !=null }"><c:out value="${chambre.battiement }"></c:out></c:if></span><br>
				<input type="submit" name="supprimer" value="supprimer">
				<br><h5><c:out value="${ resultat}"></c:out><h5>
			</form>
		</div>
	</div>
</div>
</body>
</html>