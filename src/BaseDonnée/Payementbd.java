package BaseDonn�e;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import JavaBeans.payement;

public class Payementbd {
	
	public Connection cn=null;
    private Statement st=null;
    private PreparedStatement ps=null;
    //le constructeur est bon;
    public Payementbd(){
         try{
         try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiceInternat1","root","root1234");
        System.out.println("connexion etablie");
        String table="payement";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){System.out.println("ceci existe deja");}
        else{
           st=cn.createStatement();
           String sql="CREATE TABLE "+table+"(id_payement VARCHAR(100) not NULL,mode VARCHAR(100) not NULL, CIN VARCHAR(100)  NULL ,etat VARCHAR(100) not NULL,tranche VARCHAR(100) not NULL,pay longblob  ,PRIMARY KEY(id_payement),"+
                  "FOREIGN KEY(CIN) REFERENCES etudiant(CIN) ON DELETE SET NULL)" ;
           st.executeUpdate(sql);
           System.out.println("table payement cree");
        }
        
    }
        catch(SQLException e){
        	e.printStackTrace();
            System.out.println("une erreur s'est produit lors de la connexion � la base de donn�e,veuillez verifier les param�tres de la base de donn�e. ");
        }
    }
    
    public ArrayList<payement> listerpaymentsv(){
		ArrayList<payement> lp=new ArrayList<payement>();
		ResultSet rs;
		PreparedStatement ps;
		try {
			ps=cn.prepareStatement("select * from payement where etat=?");
			ps.setString(1, "valid");
			rs=ps.executeQuery();
			while(rs.next()) {
				payement p=new payement();
				p.setId_payement(rs.getString(1));
				p.setMode(rs.getString(2));
				p.setCIN(rs.getString(3));
				p.setEtat(rs.getString(4));
				p.setTranche(rs.getString(5));
				lp.add(p);
			}
			return lp;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lp;
	}
    public ArrayList<payement> listerpaymentsa(){
		ArrayList<payement> lp=new ArrayList<payement>();
		ResultSet rs;
		PreparedStatement ps;
		try {
			ps=cn.prepareStatement("select * from payement where etat=?");
			ps.setString(1, "en attente");
			rs=ps.executeQuery();
			while(rs.next()) {
				payement p=new payement();
				p.setId_payement(rs.getString(1));
				p.setMode(rs.getString(2));
				p.setCIN(rs.getString(3));
				p.setEtat(rs.getString(4));
				p.setTranche(rs.getString(5));
				lp.add(p);
			}
			return lp;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lp;
	}
    public ArrayList<payement> listerpaymentsn(){
		ArrayList<payement> lp=new ArrayList<payement>();
		ResultSet rs;
		PreparedStatement ps;
		try {
			ps=cn.prepareStatement("select * from payement where etat=?");
			ps.setString(1, "non valide");
			rs=ps.executeQuery();
			while(rs.next()) {
				payement p=new payement();
				p.setId_payement(rs.getString(1));
				p.setMode(rs.getString(2));
				p.setCIN(rs.getString(3));
				p.setEtat(rs.getString(4));
				p.setTranche(rs.getString(5));
				lp.add(p);
			}
			return lp;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lp;
	}
    public payement chercherpayement(String id_payement) {
    	PreparedStatement ps;
		payement p=new payement();
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select * from payement where id_payement=? ");
			ps.setString(1, id_payement);
			rs=ps.executeQuery();
			while(rs.next()) {
				p.setId_payement(rs.getString(1));
				p.setMode(rs.getString(2));
				p.setCIN(rs.getString(3));
				p.setEtat(rs.getString(4));
				p.setTranche(rs.getString(5));
			}
			return p;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return p;
    }
    public boolean existepayement(String id_payement,String etat) {
		PreparedStatement ps;
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select * from payement where id_payement=? and etat!=?");
			ps.setString(1, id_payement);
			ps.setString(2, etat);
			rs=ps.executeQuery();
			if(rs.next()) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
    public String validerpayement(String id_payement,String etat) {
		PreparedStatement ps;
		String  res="ce payement est d�ja valid� ou n'existe pas";
				if(existepayement(id_payement,etat)) {
					try {
						ps=cn.prepareStatement("update payement set etat=? where id_payement=?");
						ps.setString(1, etat);
						ps.setString(2, id_payement);
						ps.executeUpdate();
						ps=cn.prepareStatement("update etudiant set id_chambre=(select id_chambre from reservation where cin=(select cin from payement where id_payement=?)) where cin=(select cin from payement where id_payement=?)");
						ps.setString(1, id_payement);
						ps.setString(2, id_payement);
						ps.executeUpdate();
						ps=cn.prepareStatement("update chambre set etat=? where id_chambre=(select id_chambre from reservation where cin=(select cin from payement where id_payement=?))");
						ps.setString(1, "OCCUPEE");
						ps.setString(2, id_payement);
						ps.executeUpdate();
						res= "payement valid�";
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			return res;
	
	}
    public String invaliderpayement(String id_payement,String etat) {
		PreparedStatement ps;
		ResultSet rs=null;
		String  res="ce payement est d�ja invalid� ou n'existe pas";
				if(existepayement(id_payement,etat)) {
					try {
						ps=cn.prepareStatement("update payement set etat=? where id_payement=?");
						ps.setString(1, etat);
						ps.setString(2, id_payement);
						ps.executeUpdate();
						ps=cn.prepareStatement("select id_chambre from etudiant where cin=(select cin from payement where id_payement=?)");
						ps.setString(1, id_payement);
						rs=ps.executeQuery();
						if(rs.next()) {
							if(rs.getString(1)!=null){
								ps=cn.prepareStatement("update chambre set etat=? where id_chambre=(select id_chambre from reservation where cin=(select cin from payement where id_payement=?))");
								ps.setString(1, "RESERVEE");
								ps.setString(2, id_payement);
								ps.executeUpdate();
								ps=cn.prepareStatement("update etudiant set id_chambre=? where cin=(select cin from payement where id_payement=?)");
								ps.setString(1, null);
								ps.setString(2, id_payement);
								ps.executeUpdate();
							}
							
						}
						res= "payement invalid�";
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			return res;
	
	}
    
    public String insertpaiement(String cin, String mode,String path,String tranche) {
    	String resultat="";
    	File f=new File(path);
    	String id_payement=cin+mode;
    	if(chercherpayement(id_payement).getId_payement()!=null){
    		resultat="vous avez deja effectuez un payement de type:"+mode;
    		
    	}
    	else 
    	{if(f.exists()) {
    		try {
				ps=cn.prepareStatement("insert into payement value(?,?,?,?,?,null)");
				ps.setString(1, id_payement.toUpperCase());
				ps.setString(2, mode.toUpperCase());
				ps.setString(3, cin.toUpperCase());
				ps.setString(4,"EN ATTENTE");
				ps.setString(5, tranche);
				ps.executeUpdate();
				insererdocument(id_payement,path);
				resultat="payement effectu�";
				
				
			} 
    		catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    		else {
	    		return "le re�u n'existe pas dans l'emplacement specifi�";
	    	}
    	
    	}
    	return resultat;
    }
    
    
    public void insererdocument(String id_payement,String path) {
		PreparedStatement ps=null;
		FileInputStream input=null;
		try {
			File f=new File(path);
			System.out.println(f.exists());
			ps=cn.prepareStatement("update payement set pay=? where id_payement=?");
			input=new FileInputStream(f);
			ps.setString(2, id_payement);
			ps.setBlob(1, input);
			int n=ps.executeUpdate();
			System.out.println(n);
			
		}
		catch(Exception e) {
			System.out.println("erreur");
		}
		
	}
    public String messagepayement(String cin) {
    	String resultat="";
    	ResultSet rs=null;
    	try {
    		Reservationbd r=new Reservationbd();
    		if(r.messagereservation(cin).equalsIgnoreCase("votre reservation a �t� valid� par l'admin,veuillez passez au menu payement.")) {
    			System.out.println("hna1");
    			ps=cn.prepareStatement("select * from payement where cin=?");
    			ps.setString(1, cin);
    			rs=ps.executeQuery();
    			if(rs.next()) {
    				if(rs.getString(4).equalsIgnoreCase("Valid")) {
    					resultat="votre payment a �t� valid� par l'administrateur";
    				}
    				else if(rs.getString(4).equalsIgnoreCase("En attente")) {
    					resultat="votre payement est en attente de la validation de l'administrateur";
    				}
    				else if(rs.getString(4).equalsIgnoreCase("non valide")) {
    					resultat="votre payement n'est pas valider par l'administrateur ,veuillez effectuer une autre payement";
    				}
    			}
    			else {
    				resultat="aucun payement effectu�e,veuillez effectuez votre payement.";
    			}
    		}
    		else {
    			resultat="le payement  n'est pas possible avant la validation de la reservation";
    		}
    		return resultat;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return resultat;
    	
    }
    public String payer(String mode,String cin,String path,String tranche) {
    	
    	String resultat="";
    	if(messagepayement(cin).equalsIgnoreCase("aucun payement effectu�e,veuillez effectuez votre payement.")) {
    		resultat=insertpaiement( cin,  mode, path, tranche);
    		
    	}
    	else if(messagepayement(cin).equalsIgnoreCase("votre payement n'est pas valider par l'administrateur ,veuillez effectuer une autre payement")) {
    		File f=new File(path);
    		if(f.exists()) {
    			insererdocumentnv(cin+mode,path);
    			resultat="payement effectu�";
    		}
    		else {
    			resultat="le fichie n'existe pas dans l'emplacement specifi�";
    		}
    	}
    	else if(messagepayement(cin).equalsIgnoreCase("votre payement est en attente de la validation de l'administrateur")||messagepayement(cin).equalsIgnoreCase("votre payment a �t� valid� par l'administrateur")) {
    		resultat="vous avez deja effectu� un payement";
    	}
    	else {
    		resultat="la reservation  n'est pas valid�,pour effectuer un payement";
    	}
    	return resultat;
    }
    public void insererdocumentnv(String id_payement,String path) {
		PreparedStatement ps=null;
		FileInputStream input=null;
		try {
			File f=new File(path);
			System.out.println(f.exists());
			ps=cn.prepareStatement("update payement set pay=?,etat=? where id_payement=?");
			input=new FileInputStream(f);
			ps.setString(3, id_payement.toUpperCase());
			ps.setString(2, "EN ATTENTE");
			ps.setBlob(1, input);
			int n=ps.executeUpdate();
			System.out.println(n);
			
		}
		catch(Exception e) {
			System.out.println("erreur");
		}
		
	}
    public payement chercherpayementcin(String cin) {
    	PreparedStatement ps=null;
    	payement p=null;
    	ResultSet rs=null;
    	try {
			ps=cn.prepareStatement("select * from payement where cin=? ");
			ps.setString(1, cin);
			rs=ps.executeQuery();
			while(rs.next()) {
				p=new payement();
				p.setId_payement(rs.getString(1));
				p.setMode(rs.getString(2));
				p.setCIN(rs.getString(3));
				p.setEtat(rs.getString(4));
				p.setTranche(rs.getString(5));
	    }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return p;
		
    }
    public void closeconnection() {
		 try {
			cn.close();
			System.out.print("connexion ferm�");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }


    
}
