package BaseDonn�e;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import JavaBeans.Etudiant;

public class Etudiantbd {
	public Connection cn=null;
    private Statement st=null;
    
    //le constructeur est bon;
    public Etudiantbd(){
         try{
         try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiceInternat1","root","root1234");
        System.out.println("connexion etablie");
        String table="etudiant";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){System.out.println("ceci existe deja");}
        else{
           st=cn.createStatement();
           String sql="CREATE TABLE "+table+"(nom VARCHAR(100) not NULL,prenom VARCHAR(100) not NULL, CIN VARCHAR(100) not NULL ,login VARCHAR(100) ,password VARCHAR(100),id_chambre VARCHAR(100) ,sexe VARCHAR(100),image mediumblob ,PRIMARY KEY(CIN),"+
                  "FOREIGN KEY(id_chambre) REFERENCES chambre(id_chambre) ON DELETE SET NULL)" ;
           st.executeUpdate(sql);
           System.out.println("table Etudiant cree");
        }
        
    }
        catch(SQLException e){
        	e.printStackTrace();
            System.out.println("une erreur s'est produit lors de la connexion � la base de donn�e,veuillez verifier les param�tres de la base de donn�e. ");
        }
    }
    public Etudiant chercheretudiant(String cin) {
    	PreparedStatement ps;
		Etudiant e=new Etudiant();
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select nom,prenom,cin,sexe,id_chambre from etudiant where cin=? ");
			ps.setString(1, cin);
			rs=ps.executeQuery();
			String id=null;
			while(rs.next()) {
				e.setCIN(rs.getString(3));
				e.setNom(rs.getString(1));
				e.setPrenom(rs.getString(2));
				e.setSexe(rs.getString(4));
				id=rs.getString(5);
				System.out.println(id);
			}
			if(id!=null) {
				ps=cn.prepareStatement("select n_chambre,battiement,type from chambre where id_chambre=? ");
				ps.setString(1, id);
				rs=ps.executeQuery();
				while(rs.next()) {
					e.setBattiement(rs.getString(2));
					e.setNumero_chambre(rs.getInt(1));
					e.setType_chambre(rs.getString(3));}
				
			}
			return e;
		} catch (SQLException f) {
			// TODO Auto-generated catch block
			f.printStackTrace();
		}
		return e;
    }
    /*---------------------------------------------------------------------
     ----------------------------------------------------------------------
     */
    public ArrayList<Etudiant> listerEtudiant(){
		ArrayList<Etudiant> lc=new ArrayList<Etudiant>();
		ResultSet rs,rs1;
		PreparedStatement ps;
		try {
			st=cn.createStatement();
			rs=st.executeQuery("Select * from etudiant");
			while(rs.next()) {
				System.out.println("1");
				Etudiant e=new Etudiant();
				e.setCIN(rs.getString(3));
				e.setNom(rs.getString(1));
				e.setPrenom(rs.getString(2));
				e.setSexe(rs.getString(7));
				String id=rs.getString(6);
				if(id!=null) {
					ps=cn.prepareStatement("select n_chambre,battiement from chambre where id_chambre=? ");
					ps.setString(1, id);
					rs1=ps.executeQuery();
					while(rs1.next()) {
						e.setBattiement(rs1.getString(2));
						e.setNumero_chambre(rs1.getInt(1));}
					
				}
				lc.add(e);
				System.out.println("dd");
			}
			return lc;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lc;
	}
    public String deleteetudiant(String cin) {
		PreparedStatement ps;
		PreparedStatement ps1;
		ResultSet rs=null;
		String res="cet etudiant n'existe pas pour le supprimer";
		
		try {
			ps=cn.prepareStatement("select id_chambre from etudiant where cin=?");
			ps.setString(1, cin);
			rs=ps.executeQuery();
			if(rs.next()) {
				if(rs.getString(1)!=null) {
					ps=cn.prepareStatement("update chambre set etat=? where id_chambre=?");
					ps.setString(1, "LIBRE");
					ps.setString(2, rs.getString(1));
					ps.executeUpdate();
					
				}
				else {
					ps1=cn.prepareStatement("select id_chambre from reservation where cin=?");
					ps1.setString(1, cin);
					rs=ps1.executeQuery();
					if(rs.next()) {
						ps1=cn.prepareStatement("update chambre set etat=? where id_chambre=(select id_chambre from reservation where cin=?)");
						ps1.setString(1, "LIBRE");
						ps1.setString(2, cin);
						ps1.executeUpdate();
					}
				}
				ps=cn.prepareStatement("delete from etudiant where cin=?");
				ps.setString(1, cin);
				ps.executeUpdate();
				res="etudiant supprim�";
			}
			else {
				res="etudiant n'existe pas";
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
    public boolean existetud(String cin) {
		PreparedStatement ps;
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select * from etudiant where cin=? ");
			ps.setString(1, cin);
			rs=ps.executeQuery();
			if(rs.next()) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public String AjouterEtudiant(String cin,String nom,String prenom) {
		PreparedStatement ps;
		String res="cette etudiant existe d�ja";
				if(!existetud(cin)) {
					System.out.println("kaynch");
					try {
						ps=cn.prepareStatement("insert into etudiant value(?,?,?,null,null,null,null,null)");
						ps.setString(1, nom.toUpperCase());
						ps.setString(2, prenom.toUpperCase());
						ps.setString(3, cin.toUpperCase());
						System.out.println("d");
						ps.executeUpdate();
						return "etudiant ajout�";
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					return "une erreur s'est produite";
					
				}
			
			
		return res;
	
	}
	public void insererdocument(String cin,String path) {
		PreparedStatement ps=null;
		FileInputStream input=null;
		try {
			File f=new File(path);
			System.out.println(f.exists());
			ps=cn.prepareStatement("update etudiant set image=? where cin=?");
			input=new FileInputStream(f);
			ps.setString(2, cin);
			ps.setBlob(1, input);
			int n=ps.executeUpdate();
			System.out.println(n);
			
		}
		catch(Exception e) {
			System.out.println("erreur");
		}
		
	}
	
	public Etudiant chercheretudiantlogin(String login) {
    	PreparedStatement ps;
		Etudiant e=new Etudiant();
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select nom,prenom,cin,sexe,id_chambre from etudiant where login=? ");
			ps.setString(1, login);
			rs=ps.executeQuery();
			String id=null;
			while(rs.next()) {
				e.setCIN(rs.getString(3));
				e.setNom(rs.getString(1));
				e.setPrenom(rs.getString(2));
				e.setSexe(rs.getString(4));
				id=rs.getString(5);
				
			}
			if(id!=null) {
				ps=cn.prepareStatement("select n_chambre,battiement,type from chambre where id_chambre=? ");
				ps.setString(1, id);
				rs=ps.executeQuery();
				while(rs.next()) {
					e.setBattiement(rs.getString(2));
					e.setNumero_chambre(rs.getInt(1));
					e.setType_chambre(rs.getString(3));}
			}
			return e;
		} catch (SQLException f) {
			// TODO Auto-generated catch block
			f.printStackTrace();
		}
		return e;
    }
	 public boolean existlog(String login,String password) {
	    	ResultSet rs;
	    	PreparedStatement ps=null;
	    	try {
				ps=cn.prepareStatement("select login,password from etudiant where login=? AND password=?");
				ps.setString(1, login);
				ps.setString(2, password);
				rs=ps.executeQuery();
				if(rs.next()) {
					return true;
				}
				
			} catch (SQLException e) {
				System.out.println("erreur ici");
				e.printStackTrace();
			}
	    	return false;
	    	
	    }
	 public String inscription(String cin,String nom,String prenom,String sexe,String email,String password,String path) {
		 PreparedStatement ps1, ps=null;
		 String resultat="";
		 ResultSet rs=null;
		 try {
			ps=cn.prepareStatement("select login from etudiant where cin=? and nom=? and prenom=?");
			ps.setString(1, cin);
			ps.setString(2, nom);
			ps.setString(3, prenom);
			rs=ps.executeQuery();
			if(rs.next()) {
				if(rs.getString(1)==null) {
					ps=cn.prepareStatement("select login from etudiant where login=?");
					ps.setString(1, email);
					rs=ps.executeQuery();
					if(rs.next()) {
						if(rs.getString(1).equals(email)) {
							resultat="cet email est deja utilis�";
						}
						else {
							if(password.length()<8) {
								resultat="veuillez saisir un  mot de passe dont la taille est >=8";
							}
							else {
								ps=cn.prepareStatement("update etudiant set login=?,sexe=?,password=? where cin=?");
								ps.setString(1, email);
								ps.setString(2, sexe.toUpperCase());
								ps.setString(3, password);
								ps.setString(4, cin);
								ps.executeUpdate();
								insererdocument(cin,path);
								resultat="inscription valide";
							}
						}
					}
					else {
						if(password.length()<8) {
							resultat="veuillez saisir un  mot de passe dont la taille est >=8";
						}
						else {
							ps1=cn.prepareStatement("update etudiant set login=?,sexe=?,password=? where cin=?");
							ps1.setString(1, email);
							ps1.setString(2, sexe.toUpperCase());
							ps1.setString(3, password);
							ps1.setString(4, cin);
							ps1.executeUpdate();
							insererdocument(cin,path);
							resultat="inscription valide";
						}
					}
				}
				else {
					resultat="vous �tes d�ja inscrit";
				}
				
					
				
				
			}
			else {
				resultat="vous n'�tes pas autoris� pour s'inscrire";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return resultat;
	 }

	 public void closeconnection() {
		 try {
			cn.close();
			System.out.print("connexion ferm�");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }


}
		
		
	


