package BaseDonn�e;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import JavaBeans.Reclamation;

public class Reclamationbd {
	public Connection cn=null;
    private Statement st=null;
    private PreparedStatement ps=null;
    //le constructeur est bon;
    public Reclamationbd(){
         try{
         try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiceInternat1","root","root1234");
        System.out.println("connexion etablie");
        String table="reclamation";
        DatabaseMetaData db=cn.getMetaData();
        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
        if(tables.next()){System.out.println("ceci existe deja");}
        else{
           st=cn.createStatement();
           String sql="CREATE TABLE "+table+"(id_reclamation INTEGER not NULL AUTO_INCREMENT,type  VARCHAR(100) not NULL ,description VARCHAR(1000) not NULL,etat VARCHAR(100) not NULL ,CIN VARCHAR(100) not  NULL ,PRIMARY KEY(id_reclamation),"+
                  "FOREIGN KEY(CIN) REFERENCES etudiant(CIN) ON DELETE CASCADE)" ;
           st.executeUpdate(sql);
           System.out.println("table reclamation cree");
        }
        
    }
        catch(SQLException e){
        	e.printStackTrace();
            System.out.println("une erreur s'est produit lors de la connexion � la base de donn�e,veuillez verifier les param�tres de la base de donn�e. ");
        }
    }
    public ArrayList<Reclamation> listerreclamationv(){
		ArrayList<Reclamation> lr=new ArrayList<Reclamation>();
		ResultSet rs;
		PreparedStatement ps;
		try {
			ps=cn.prepareStatement("select * from reclamation where etat=?");
			ps.setString(1, "valid");
			rs=ps.executeQuery();
			while(rs.next()) {
				Reclamation r=new Reclamation();
				r.setId_reclamation(rs.getInt(1));
				r.setType(rs.getString(2));
				r.setDescription(rs.getString(3));
				r.setEtat(rs.getString(4));
				r.setCin(rs.getString(5));
				lr.add(r);
			}
			return lr;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lr;
	}
    public ArrayList<Reclamation> listerreclamationa(){
		ArrayList<Reclamation> lr=new ArrayList<Reclamation>();
		ResultSet rs;
		PreparedStatement ps;
		try {
			ps=cn.prepareStatement("select * from reclamation where etat=?");
			ps.setString(1, "en attente");
			rs=ps.executeQuery();
			while(rs.next()) {
				Reclamation r=new Reclamation();
				r.setId_reclamation(rs.getInt(1));
				r.setType(rs.getString(2));
				r.setDescription(rs.getString(3));
				r.setEtat(rs.getString(4));
				r.setCin(rs.getString(5));
				lr.add(r);
			}
			return lr;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lr;
	}
    public ArrayList<Reclamation> listerreclamationn(){
		ArrayList<Reclamation> lr=new ArrayList<Reclamation>();
		ResultSet rs;
		PreparedStatement ps;
		try {
			ps=cn.prepareStatement("select * from reclamation where etat=?");
			ps.setString(1, "non valide");
			rs=ps.executeQuery();
			while(rs.next()) {
				Reclamation r=new Reclamation();
				r.setId_reclamation(rs.getInt(1));
				r.setType(rs.getString(2));
				r.setDescription(rs.getString(3));
				r.setEtat(rs.getString(4));
				r.setCin(rs.getString(5));
				lr.add(r);
			}
			return lr;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lr;
	}
    public Reclamation chercherreclamation(int id_reclamation) {
    	PreparedStatement ps;
		Reclamation r=new Reclamation();
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select * from reclamation where id_reclamation=? ");
			ps.setInt(1, id_reclamation);;
			rs=ps.executeQuery();
			while(rs.next()) {
				r.setId_reclamation(rs.getInt(1));
				r.setType(rs.getString(2));
				r.setDescription(rs.getString(3));
				r.setCin(rs.getString(5));
				r.setEtat(rs.getString(4));
			}
			return r;
		} catch (SQLException f) {
			// TODO Auto-generated catch block
			f.printStackTrace();
		}
		return r;
    }
    public String validerreclamation(int id_reclamation,String etat) {
		PreparedStatement ps;
		String  res="cette reclamation est d�ja valid�  ou n'existe pas";
		String	res1="cette reclamation est d�ja invalid�  ou n'existe pas" ;
				if(existreclamation(id_reclamation,etat)) {
					try {
						ps=cn.prepareStatement("update reclamation set etat=? where id_reclamation=?");
						ps.setString(1, etat);
						ps.setInt(2, id_reclamation);
						ps.executeUpdate();
						res= "reclamation valid�";
						res1="reclamation est invalid�";
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
			
			
			
		if(etat.equalsIgnoreCase("valid")) {
			return res;
		}
			return res1;
	
	}
    public boolean existreclamation(int id_reclamation,String etat) {
		PreparedStatement ps;
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select * from reclamation where id_reclamation=? and etat!=?");
			ps.setInt(1, id_reclamation);
			ps.setString(2, etat);
			rs=ps.executeQuery();
			if(rs.next()) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
		
	}
    public String reclamation(String cin,String type,String description,String etat) {
    	ResultSet rs=null;
    	String resultat="";
    	try {
			ps=cn.prepareStatement("select id_chambre from etudiant where cin=?");
			ps.setString(1, cin);
			rs=ps.executeQuery();
			if(rs.next()) {
				if(rs.getString(1)!=null) {
					ps=cn.prepareStatement("insert into reclamation value(null,?,?,?,?)");
					ps.setString(1, type.toUpperCase());
					ps.setString(2, description);
					ps.setString(3, etat);
					ps.setString(4, cin);
					ps.executeUpdate();
					resultat="reclamation effectu�";
				}
				else {
					resultat="vous n'avez pas complet� votre inscription pour effectuer une reclamation";
				}
			}
			return resultat;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return resultat;
    }
    public void closeconnection() {
		 try {
			cn.close();
			System.out.print("connexion ferm�");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }

}
