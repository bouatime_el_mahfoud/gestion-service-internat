package BaseDonn�e;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import JavaBeans.Fichier;

public class Piecebd {
	
		public Connection cn=null;
	    private Statement st=null;
	    private PreparedStatement ps=null;
	    //le constructeur est bon;
	    public Piecebd(){
	         try{
	         try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiceInternat1","root","root1234");
	        System.out.println("connexion etablie");
	        String table="fichier";
	        DatabaseMetaData db=cn.getMetaData();
	        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
	        if(tables.next()){System.out.println("ceci existe deja");}
	        else{
	           st=cn.createStatement();
	           String sql="CREATE TABLE "+table+"(id_fichier VARCHAR(100) not NULL,type VARCHAR(100) not NULL, CIN VARCHAR(100)  NULL ,etat VARCHAR(100) not NULL,fich longblob ,PRIMARY KEY(id_fichier),"+
	                  "FOREIGN KEY(CIN) REFERENCES etudiant(CIN) ON DELETE SET NULL)" ;
	           st.executeUpdate(sql);
	           System.out.println("table fichier cree");
	        }
	        
	    }
	        catch(SQLException e){
	        	e.printStackTrace();
	            System.out.println("une erreur s'est produit lors de la connexion � la base de donn�e,veuillez verifier les param�tres de la base de donn�e. ");
	        }
	    }
	   
	    public ArrayList<Fichier> listerfichiersv(){
			ArrayList<Fichier> lf=new ArrayList<Fichier>();
			ResultSet rs;
			PreparedStatement ps;
			try {
				ps=cn.prepareStatement("select * from fichier where etat=?");
				ps.setString(1, "valid");
				rs=ps.executeQuery();
				while(rs.next()) {
					Fichier f=new Fichier();
					f.setId_fichier(rs.getString(1));
					f.setType(rs.getString(2));
					f.setCIN(rs.getString(3));
					f.setEtat(rs.getString(4));
					lf.add(f);
				}
				return lf;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return lf;
		}
	    public ArrayList<Fichier> listerfichiersa(){
			ArrayList<Fichier> lf=new ArrayList<Fichier>();
			ResultSet rs;
			PreparedStatement ps;
			try {
				ps=cn.prepareStatement("select * from fichier where etat=?");
				ps.setString(1, "en attente");
				rs=ps.executeQuery();
				while(rs.next()) {
					Fichier f=new Fichier();
					f.setId_fichier(rs.getString(1));
					f.setType(rs.getString(2));
					f.setCIN(rs.getString(3));
					f.setEtat(rs.getString(4));
					lf.add(f);
				}
				return lf;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return lf;
		}
	    public ArrayList<Fichier> listerfichiersn(){
			ArrayList<Fichier> lf=new ArrayList<Fichier>();
			ResultSet rs;
			PreparedStatement ps;
			try {
				ps=cn.prepareStatement("select * from fichier where etat=?");
				ps.setString(1, "non valide");
				rs=ps.executeQuery();
				while(rs.next()) {
					Fichier f=new Fichier();
					f.setId_fichier(rs.getString(1));
					f.setType(rs.getString(2));
					f.setCIN(rs.getString(3));
					f.setEtat(rs.getString(4));
					lf.add(f);
				}
				return lf;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return lf;
		}
	    public Fichier chercherfichier(String id_fichier) {
	    	PreparedStatement ps;
			Fichier f=new Fichier();
			ResultSet rs;
			try {
				ps=cn.prepareStatement("select * from fichier where id_fichier=? ");
				ps.setString(1, id_fichier);
				rs=ps.executeQuery();
				while(rs.next()) {
					f.setId_fichier(rs.getString(1));
					f.setCIN(rs.getString(3));
					f.setType(rs.getString(2));
					f.setEtat(rs.getString(4));
				}
				return f;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return f;
	    }
	    public boolean existefichier(String id_fichier,String etat) {
			PreparedStatement ps;
			ResultSet rs;
			try {
				ps=cn.prepareStatement("select * from fichier where id_fichier=? and etat!=?");
				ps.setString(1, id_fichier);
				ps.setString(2, etat);
				rs=ps.executeQuery();
				if(rs.next()) {
					return true;
				}
				return false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
			
		}
	    public String validerFichier(String id_fichier,String etat) {
			PreparedStatement ps;
			String  res="ce fichier est d�ja valid� ou n'existe pas";
			String	res1="ce  fichier est d�ja invalid� ou n'existe pas" ;
					if(existefichier(id_fichier,etat)) {
						try {
							ps=cn.prepareStatement("update fichier set etat=? where id_fichier=?");
							ps.setString(1, etat);
							ps.setString(2, id_fichier);
							ps.executeUpdate();
							res= "fichier valid�";
							res1="fichier est invalid�";
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					}
					
				
				
				
			if(etat.equalsIgnoreCase("valid")) {
				return res;
			}
				return res1;
		
		}
	    public String insertpiece(String cin, String type,String path) {
	    	String resultat="";
	    	File f=new File(path);
	    	String id_fichier=cin+type;
	    	 
	    	if(f.exists()) {
	    		try {
					ps=cn.prepareStatement("insert into fichier value(?,?,?,?,null)");
					ps.setString(1, id_fichier.toUpperCase());
					ps.setString(2, type.toUpperCase());
					ps.setString(3, cin.toUpperCase());
					ps.setString(4,"EN ATTENTE");
					ps.executeUpdate();
					insererdocument(id_fichier,path);
					resultat="fichier depos�";
					
					
				} 
	    		catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	else {
		    		return "le fichie n'existe pas dans l'emplacement specifi�";
		    	}
	    	return resultat;
	    	}
	    	
	    
	    
	    
	    public void insererdocument(String id_fichier,String path) {
			PreparedStatement ps=null;
			FileInputStream input=null;
			try {
				File f=new File(path);
				System.out.println(f.exists());
				ps=cn.prepareStatement("update fichier set fich=? where id_fichier=?");
				input=new FileInputStream(f);
				ps.setString(2, id_fichier);
				ps.setBlob(1, input);
				int n=ps.executeUpdate();
				System.out.println(n);
				
			}
			catch(Exception e) {
				System.out.println("erreur");
			}
			
		}
	    public String messagedepot(String cin) {
	    	String resultat="";
	    	ResultSet rs=null;
	    	PreparedStatement ps=null;
	    	try {
				ps=cn.prepareStatement("select * from fichier where CIN=?");
				ps.setString(1, cin);
				rs=ps.executeQuery();
				String res1,res2;
			
				if(rs.next()) {
					res1=rs.getString(4);
					
					if(rs.next()) {
						
						res2=rs.getString(4);
						if(res1.equalsIgnoreCase("Valid")&&res2.equalsIgnoreCase("Valid")) {
							resultat="votre depot � �t� valid� par l'administrateur veuillez passez au menu reservation";
						}
						else if(res1.equalsIgnoreCase("Valid")&&res2.equalsIgnoreCase("En attente")) {
							resultat="votre depot est en attente de la validation de l'administrateur";
						}
						else if(res1.equalsIgnoreCase("Valid")&&res2.equalsIgnoreCase("non valide")) {
							resultat="votre depot depot n'est pas valid� ,veuilez verifier a quali� et le contenu avant de redeposer les pi�ces";
						}
						else if(res1.equalsIgnoreCase("en attente")&&res2.equalsIgnoreCase("Valid")) {
							resultat="votre depot est en attente de la validation de l'administrateur";
						}
						else if(res1.equalsIgnoreCase("en attente")&&res2.equalsIgnoreCase("En attente")) {
							resultat="votre depot est en attente de la validation de l'administrateur";
						}
						else if(res1.equalsIgnoreCase("en attente")&&res2.equalsIgnoreCase("non valide")) {
							resultat="votre depot depot n'est pas valid� ,veuilez verifier a quali� et le contenu avant de redeposer les pi�ces";
						}
						else if(res1.equalsIgnoreCase("non valide")&&res2.equalsIgnoreCase("Valid")) {
							resultat="votre depot depot n'est pas valid� ,veuilez verifier a quali� et le contenu avant de redeposer les pi�ces";
						}
						else if(res1.equalsIgnoreCase("non valide")&&res2.equalsIgnoreCase("En attente")) {
							resultat="votre depot depot n'est pas valid� ,veuilez verifier a quali� et le contenu avant de redeposer les pi�ces";
						}
						else if(res1.equalsIgnoreCase("non valide")&&res2.equalsIgnoreCase("non valide")) {
							resultat="votre depot depot n'est pas valid� ,veuilez verifier a quali� et le contenu avant de redeposer les pi�ces";
						}
					}
					else {
							if(res1.equalsIgnoreCase("Valid")) {
								resultat="aucun depot de fichiers a �t� effectuez";
							}
							if(res1.equalsIgnoreCase("non valide")) {
								resultat="aucun depot de fichiers a �t� effectuez";
							}
							if(res1.equalsIgnoreCase("en attente")) {
								resultat="aucun depot de fichiers a �t� effectuez";
							}
						}
					}
					
				
				else {
					resultat="aucun depot de fichiers a �t� effectuez";
				}
				
				return resultat;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	return resultat;
	    	
	    }
	    public String deposerficher(String cin,String type,String path) {
	    	String choix=messagedepot(cin);
	    	String resultat="";
	    	if(choix.equalsIgnoreCase("aucun depot de fichiers a �t� effectuez")) {
	    		
	    		resultat=insertpiece(cin,type,path);
	    		
	    	}
	    	else if(choix.equalsIgnoreCase("votre depot depot n'est pas valid� ,veuilez verifier a quali� et le contenu avant de redeposer les pi�ces")) {
	    		File f=new File(path);
	    		if(f.exists()) {
	    			insererdocumentnv(cin+type,path);
	    			resultat="fichier depos�";
	    		}
	    		else {
	    			resultat="le fichie n'existe pas dans l'emplacement specifi�";
	    		}
	    	}
	    	else {
	    		resultat="vous avez deja effectu� un depot de fichier";
	    	
	    	}
	    	return resultat;
	    }
	    public void insererdocumentnv(String id_fichier,String path) {
			PreparedStatement ps=null;
			FileInputStream input=null;
			try {
				File f=new File(path);
				System.out.println(f.exists());
				ps=cn.prepareStatement("update fichier set fich=?,etat=? where id_fichier=?");
				input=new FileInputStream(f);
				ps.setString(3, id_fichier);
				ps.setString(2, "EN ATTENTE");
				ps.setBlob(1, input);
				int n=ps.executeUpdate();
				System.out.println(n);
				
			}
			catch(Exception e) {
				System.out.println("erreur");
			}
			
		}
	    public void closeconnection() {
			 try {
				cn.close();
				System.out.print("connexion ferm�");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
	  
}

