package BaseDonn�e;

import java.sql.*;
import java.util.ArrayList;

import JavaBeans.Chambre;
public class Chambrebd {
	private Connection cn=null;
	private Statement st=null;
	public Chambrebd(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try{
       cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiceInternat1","root","root1234");
       
       System.out.println("connexion etablie");
       String table="chambre";
       DatabaseMetaData db=cn.getMetaData();
       ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
       if(tables.next()){System.out.println("ceci existe deja");}
       else{
          st=cn.createStatement();
          String sql="CREATE TABLE "+table+"(id_chambre VARCHAR(100) not NULL,type VARCHAR(100) not NULL ,battiement VARCHAR(100) not NULL,n_chambre INTEGER not NULL,etat VARCHAR(100) not NULL,PRIMARY KEY(id_chambre)"+
                 ")" ;
          st.executeUpdate(sql);
          System.out.println("table chambre cree");
       }
       
   }
       catch(SQLException e){
           System.out.println("une erreur s'est produit lors de la connexion");
           e.printStackTrace();
          
       }
   }
	public Chambre existchambre(int n_chambre,String battiement) {
		PreparedStatement ps;
		Chambre c=new Chambre();
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select * from chambre where n_chambre=? and battiement=?");
			ps.setInt(1, n_chambre);
			ps.setString(2, battiement);
			rs=ps.executeQuery();
			while(rs.next()) {
				c.setId_chambre(rs.getString(1));
				c.setEtat(rs.getString(5));
				c.setBattiement(rs.getString(3));
				c.setN_chambre(rs.getInt(4));
				c.setType(rs.getString(2));
			}
			return c;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    		return c;
    	
		
		
		
		
	}
	public boolean existcham(int n_chambre,String battiement) {
		PreparedStatement ps;
		ResultSet rs;
		boolean b=false;
		try {
			ps=cn.prepareStatement("select * from chambre where n_chambre=? and battiement=?");
			ps.setInt(1, n_chambre);
			ps.setString(2, battiement);
			rs=ps.executeQuery();
			if(rs.next()) {
				b= true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		return b;
    	
		
	}
	public ArrayList<Chambre> listerchambre(){
		ArrayList<Chambre> lc=new ArrayList<Chambre>();
		ResultSet rs;
		try {
			st=cn.createStatement();
			rs=st.executeQuery("Select * from chambre");
			while(rs.next()) {
				Chambre c=new Chambre();
				c.setId_chambre(rs.getString(1));
				c.setBattiement(rs.getString(3));
				c.setType(rs.getString(2));
				c.setN_chambre(rs.getInt(4));
				c.setEtat(rs.getString(5));
				lc.add(c);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		return lc;
    	}
	
	public String delete(String id_chambre) {
		PreparedStatement ps;
		String res="cette chambre n'existe pas pour la supprimer";
		
		try {
			ps=cn.prepareStatement("delete from chambre where id_chambre=?");
			ps.setString(1, id_chambre);
			int r=ps.executeUpdate();
			if(r!=0) {
				return "la chambre est supprim�";
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	public String Ajouterchambre(int n_chambre,String type,String battiement) {
		PreparedStatement ps;
		String res="cette chambre existe d�ja";
		if(n_chambre>0) {
			if(type.equalsIgnoreCase("simple")||type.equalsIgnoreCase("triple")||type.equalsIgnoreCase("double")){
				if(!existcham(n_chambre,battiement)) {
					try {
						ps=cn.prepareStatement("insert into chambre value(?,?,?,?,?)");
						ps.setString(1, (battiement+n_chambre).toUpperCase());
						ps.setString(2, type.toUpperCase());
						ps.setString(3, battiement.toUpperCase());
						ps.setString(5, "LIBRE");
						ps.setInt(4, n_chambre);
						ps.executeUpdate();
						return "chambre ajout�";
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					return "une erreur s'est produite";
					
				}
			}
			else {
				return "les types valides sont:simple,double,triple";
			}
			
		return res;
	
	}
		else return "veuillez saisir un numero_chambre strictement positive";
}
	public Chambre existchambre1(String id_chambre) {
		PreparedStatement ps;
		Chambre c=new Chambre();
		ResultSet rs;
		try {
			ps=cn.prepareStatement("select * from chambre where id_chambre=? ");
			
			ps.setString(1, id_chambre);
			rs=ps.executeQuery();
			while(rs.next()) {
				c.setId_chambre(rs.getString(1));
				c.setEtat(rs.getString(5));
				c.setBattiement(rs.getString(3));
				c.setN_chambre(rs.getInt(4));
				c.setType(rs.getString(2));
			}
			return c;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c;
		
	}
	public void closeconnection() {
		 try {
			cn.close();
			System.out.print("connexion ferm�");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
}
