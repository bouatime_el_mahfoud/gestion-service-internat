package BaseDonn�e;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import JavaBeans.Reservation;
import BaseDonn�e.Piecebd;

public class Reservationbd {
	
		public Connection cn=null;
	    private Statement st=null;
	    private PreparedStatement ps=null;
	    
	    public Reservationbd(){
	         try{
	         try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiceInternat1","root","root1234");
	        System.out.println("connexion etablie");
	        String table="reservation";
	        DatabaseMetaData db=cn.getMetaData();
	        ResultSet tables=db.getTables(null, null, table.toUpperCase(),null);
	        if(tables.next()){System.out.println("ceci existe deja");}
	        else{
	           st=cn.createStatement();
	           String sql="CREATE TABLE "+table+"(id_reservation INTEGER not NULL AUTO_INCREMENT,id_chambre  VARCHAR(100) not NULL ,etat VARCHAR(100) not NULL ,CIN VARCHAR(100) not  NULL UNIQUE ,PRIMARY KEY(id_reservation),"+
	                  "FOREIGN KEY(CIN) REFERENCES etudiant(CIN) ON DELETE CASCADE,FOREIGN KEY(id_chambre) REFERENCES chambre(id_chambre) ON DELETE CASCADE)" ;
	           st.executeUpdate(sql);
	           System.out.println("table reservation cree");
	        }
	        
	    }
	        catch(SQLException e){
	        	e.printStackTrace();
	            System.out.println("une erreur s'est produit lors de la connexion � la base de donn�e,veuillez verifier les param�tres de la base de donn�e. ");
	        }
	    }
	    
	    public ArrayList<Reservation> listerreservationv(){
			ArrayList<Reservation> lr=new ArrayList<Reservation>();
			ResultSet rs;
			PreparedStatement ps;
			try {
				ps=cn.prepareStatement("select * from reservation where etat=?");
				ps.setString(1, "valid");
				rs=ps.executeQuery();
				while(rs.next()) {
					Reservation r=new Reservation();
					r.setId_reservation(rs.getInt(1));
					r.setId_chambre(rs.getString(2));
					r.setCIN(rs.getString(4));
					r.setEtat(rs.getString(3));
					lr.add(r);
				}
				return lr;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return lr;
		}
	   
	    public ArrayList<Reservation> listerreservationa(){
			ArrayList<Reservation> lr=new ArrayList<Reservation>();
			ResultSet rs;
			PreparedStatement ps;
			try {
				ps=cn.prepareStatement("select * from reservation where etat=?");
				ps.setString(1, "en attente");
				rs=ps.executeQuery();
				while(rs.next()) {
					Reservation r=new Reservation();
					r.setId_reservation(rs.getInt(1));
					r.setId_chambre(rs.getString(2));
					r.setCIN(rs.getString(4));
					r.setEtat(rs.getString(3));
					lr.add(r);
				}
				return lr;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return lr;
		}
	    public ArrayList<Reservation> listerreservationn(){
			ArrayList<Reservation> lr=new ArrayList<Reservation>();
			ResultSet rs;
			PreparedStatement ps;
			try {
				ps=cn.prepareStatement("select * from reservation where etat=?");
				ps.setString(1, "non valide");
				rs=ps.executeQuery();
				while(rs.next()) {
					Reservation r=new Reservation();
					r.setId_reservation(rs.getInt(1));
					r.setId_chambre(rs.getString(2));
					r.setCIN(rs.getString(4));
					r.setEtat(rs.getString(3));
					lr.add(r);
				}
				return lr;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return lr;
		}
	    public Reservation chercherreservation(int id_reservation) {
	    	PreparedStatement ps;
			Reservation r=new Reservation();
			ResultSet rs;
			try {
				ps=cn.prepareStatement("select * from reservation where id_reservation=? ");
				ps.setInt(1, id_reservation);;
				rs=ps.executeQuery();
				while(rs.next()) {
					r.setId_reservation(rs.getInt(1));
					r.setId_chambre(rs.getString(2));
					r.setCIN(rs.getString(4));
					r.setEtat(rs.getString(3));
					
				}
				return r;
			} catch (SQLException f) {
				// TODO Auto-generated catch block
				f.printStackTrace();
			}
			return r;
	    }
	    public String validerresrvation(int id_reservation,String etat) {
			PreparedStatement ps;
			String  res="cette resrvation est d�ja valid�  ou n'existe pas";
			ResultSet rs=null;
			
					if(existreservation(id_reservation,etat)) {
						try {
							ps=cn.prepareStatement("select cin from reservation where id_reservation=?");
							ps.setInt(1, id_reservation);
							rs=ps.executeQuery();
							if(rs.next()) {
								ps=cn.prepareStatement("select  etat from payement where cin=?");
								ps.setString(1, rs.getString(1));
								String cin=rs.getString(1);
								rs=ps.executeQuery();
								if(rs.next()) {
									if(rs.getString(1).equalsIgnoreCase("VALID")) {
										ps=cn.prepareStatement("update etudiant set id_chambre=(select id_chambre from reservation where id_reservation=? ) where cin=?");
										ps.setInt(1, id_reservation);
										ps.setString(2,cin);
										ps.executeUpdate();
										ps=cn.prepareStatement("update chambre set etat=? where id_chambre=(select id_chambre from reservation where id_reservation=?)");
										ps.setString(1, "OCCUPEE");
										ps.setInt(2, id_reservation);
										ps.executeUpdate();
									}
								}
							}
							ps=cn.prepareStatement("update reservation set etat=? where id_reservation=?");
							ps.setString(1, etat);
							ps.setInt(2, id_reservation);
							ps.executeUpdate();
							res= "reservation valid�";
							
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					}
				
				return res;
			
				
		
		}
	    public String invaliderresrvation(int id_reservation,String etat) {
			PreparedStatement ps;
			ResultSet rs=null;
			String	res1="cette reservation est d�ja invalid�  ou n'existe pas" ;
					if(existreservation(id_reservation,etat)) {
						try {
							ps=cn.prepareStatement("update reservation set etat=? where id_reservation=?");
							ps.setString(1, etat);
							ps.setInt(2, id_reservation);
							ps.executeUpdate();
							ps=cn.prepareStatement("update chambre set etat=? where id_chambre=(select id_chambre from reservation where id_reservation=?)");
							ps.setString(1, "LIBRE");
							ps.setInt(2, id_reservation);
							ps.executeUpdate();
							ps=cn.prepareStatement("select id_chambre from etudiant where cin=(select cin from reservation where id_reservation=?)");
							ps.setInt(1, id_reservation);
							rs=ps.executeQuery();
							if(rs.next()) {
								if(rs.getString(1)!=null) {
									ps=cn.prepareStatement("update etudiant set id_chambre=? where cin=(select cin from reservation where id_reservation=?)");
									ps.setString(1, null);
									ps.setInt(2, id_reservation);
									ps.executeUpdate();
								}
							}
							res1="reservation est invalid�";
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
					}
				
				return res1;
		
		}
	    public boolean existreservation(int id_reservation,String etat) {
			PreparedStatement ps;
			ResultSet rs;
			try {
				ps=cn.prepareStatement("select * from reservation where id_reservation=? and etat!=?");
				ps.setInt(1, id_reservation);
				ps.setString(2, etat);
				rs=ps.executeQuery();
				if(rs.next()) {
					return true;
				}
				return false;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
			
		}
	    public String messagereservation(String cin) {
	    	String resultat="";
	    	ResultSet rs=null;
	    	try {
	    		Piecebd p=new Piecebd();
	    		if(p.messagedepot(cin).equalsIgnoreCase("votre depot � �t� valid� par l'administrateur veuillez passez au menu reservation")) {
	    			ps=cn.prepareStatement("select * from reservation where cin=?");
	    			ps.setString(1, cin);
	    			rs=ps.executeQuery();
	    			if(rs.next()) {
	    				if(rs.getString(3).equalsIgnoreCase("Valid")) {
	    					resultat="votre reservation a �t� valid� par l'admin,veuillez passez au menu payement.";
	    				}
	    				else if(rs.getString(3).equalsIgnoreCase("En attente")) {
	    					resultat="votre reservation est en attente de la validation de l'administrateur";
	    				}
	    				else if(rs.getString(3).equalsIgnoreCase("non valide")) {
	    					resultat="votre reservation n'est pas valider par l'administrateur ,veuillez effectuer une autre reservation";
	    				}
	    			}
	    			else {
	    				resultat="aucune reservation effectu�e,veuillez effectuez une reservation.";
	    			}
	    		}
	    		else {
	    			resultat="la reservation de chambre n'est pas possible avant la validation des pieces";
	    		}
	    		return resultat;
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	return resultat;
	    	
	    }
	    public String reserverchambre(String id_chambre,String cin) {
	    	String resultat="";
	    	if(messagereservation(cin).equalsIgnoreCase("aucune reservation effectu�e,veuillez effectuez une reservation.")) {
	    		reservechambre(id_chambre,cin);
	    		resultat="reservation effectu�";
	    	}
	    	else if(messagereservation(cin).equalsIgnoreCase("votre reservation n'est pas valider par l'administrateur ,veuillez effectuer une autre reservation")) {
	    		
	    		try {
					ps=cn.prepareStatement("update reservation set id_chambre=?,etat=? where cin=?");
					ps.setString(1, id_chambre.toUpperCase());
		    		ps.setString(2, "EN ATTENTE");
		    		ps.setString(3, cin.toUpperCase());
		    		ps.executeUpdate();
		    		ps=cn.prepareStatement("update chambre set etat=? where id_chambre=?");
		    		ps.setString(1, "RESERVE");
		    		ps.setString(2, id_chambre);
		    		ps.executeUpdate();
		    		
		    		resultat="reservation effectu�";
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	else if(messagereservation(cin).equalsIgnoreCase("votre reservation est en attente de la validation de l'administrateur")||messagereservation(cin).equalsIgnoreCase("votre reservation a �t� valid� par l'admin,veuillez passez au menu payement.")) {
	    		resultat="vous avez deja effectu� une reservation";
	    	}
	    	else {
	    		resultat="le depot des pi�ces n'est pas valid�,pour effectuer une reservation";
	    	}
	    	return resultat;
	    }
	    public void reservechambre(String id_chambre,String cin) {

	    	try {
				ps=cn.prepareStatement("insert into reservation value(null,?,?,?)");
				ps.setString(1, id_chambre.toUpperCase());
				ps.setString(2, "EN ATTENTE");
				ps.setString(3, cin.toUpperCase());
				ps.executeUpdate();
				ps=cn.prepareStatement("update chambre set etat=? where id_chambre=?");
				ps.setString(1, "RESERVE");
				ps.setString(2, id_chambre);
				
				int n=ps.executeUpdate();
				System.out.println(n);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	
	    }
	
	    public ArrayList<String> listerchambre(String type){
	    	ArrayList<String> lb=new ArrayList<String>();
	    	ResultSet rs;
	    	try {
				ps=cn.prepareStatement("select id_chambre from chambre where etat=? and type=? group by battiement");
				ps.setString(1, "LIBRE");
				ps.setString(2, type);
				rs=ps.executeQuery();
				while(rs.next()) {
					lb.add(rs.getString(1));
				}
				return lb;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	return lb;
	    }
	   
	    public void closeconnection() {
			 try {
				cn.close();
				System.out.print("connexion ferm�");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }


}
