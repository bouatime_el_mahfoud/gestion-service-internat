package JavaBeans;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import BaseDonnée.Piecebd;

public class Fichier {
	private String id_fichier;
	private String type;
	private String CIN;
	private String etat;
	public String getId_fichier() {
		return id_fichier;
	}
	public void setId_fichier(String id_fichier) {
		this.id_fichier = id_fichier;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCIN() {
		return CIN;
	}
	public void setCIN(String cIN) {
		CIN = cIN;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public String messagedepot(HttpServletRequest request) {
		Piecebd pdb=new Piecebd();
		HttpSession session=request.getSession();
		String message=pdb.messagedepot((String)session.getAttribute("CIN"));
		pdb.closeconnection();
		return message;
	
	}
	public String deposerfichier(HttpServletRequest request) {
		Piecebd pbd=new Piecebd();
		HttpSession session=request.getSession();
		System.out.println(request.getParameter("cin"));
		System.out.println(request.getParameter("reglement"));
		String resultat=pbd.deposerficher((String)session.getAttribute("CIN"),"CIN", request.getParameter("cin"));
		resultat=pbd.deposerficher((String)session.getAttribute("CIN"), "REGLEMENT", request.getParameter("reglement"));
		pbd.closeconnection();
		return resultat;
	}
	
}
