package JavaBeans;


public class Reservation {
private int id_reservation;
private String id_chambre;
private String etat;
private String CIN;
public int getId_reservation() {
	return id_reservation;
}
public void setId_reservation(int id_reservation) {
	this.id_reservation = id_reservation;
}
public String getId_chambre() {
	return id_chambre;
}
public void setId_chambre(String id_chambre) {
	this.id_chambre = id_chambre;
}
public String getCIN() {
	return CIN;
}
public void setCIN(String cIN) {
	CIN = cIN;
}
public String getEtat() {
	return etat;
}
public void setEtat(String etat) {
	this.etat = etat;
}


}
