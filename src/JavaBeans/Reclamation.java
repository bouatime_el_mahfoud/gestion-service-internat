package JavaBeans;

public class Reclamation {
private int id_reclamation;
private String type;
private String description;
private String etat;
private String cin;
public int getId_reclamation() {
	return id_reclamation;
}
public void setId_reclamation(int id_reclamation) {
	this.id_reclamation = id_reclamation;
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getEtat() {
	return etat;
}
public void setEtat(String etat) {
	this.etat = etat;
}
public String getCin() {
	return cin;
}
public void setCin(String cin) {
	this.cin = cin;
}


}
