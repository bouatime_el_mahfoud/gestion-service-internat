package JavaBeans;

import java.util.ArrayList;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import BaseDonnée.Chambrebd;
import BaseDonnée.Etudiantbd;
import BaseDonnée.Payementbd;
import BaseDonnée.Piecebd;
import BaseDonnée.Reclamationbd;
import BaseDonnée.Reservationbd;

public class Administrateur {
	
	public boolean connexion(HttpServletRequest request) {
		BaseDonnée.Administrateur ad=new BaseDonnée.Administrateur();
		HttpSession session=request.getSession();
		boolean b=ad.existlog((String)session.getAttribute("login"),(String)session.getAttribute("password"));
		ad.closeconnection();
		return b;
	}
	public Chambre chercherchambre(HttpServletRequest request) {
		Chambrebd cb=new Chambrebd();
		int n_chambre=Integer.parseInt(request.getParameter("numero_chambre"));
		String battiement= request.getParameter("battiement");
		Chambre c=cb.existchambre(n_chambre, battiement);
		cb.closeconnection();
		return c;
	}
	public Chambre chercherchambre1(HttpServletRequest request) {
		Chambrebd cb=new Chambrebd();
		String id_chambre= request.getParameter("id_chambre");
		Chambre c=cb.existchambre1(id_chambre);
		cb.closeconnection();
		return c;
	}
	public Etudiant chercherEtudiant(HttpServletRequest request) {
		Etudiantbd cb=new Etudiantbd();
		Etudiant e=cb.chercheretudiant(request.getParameter("CIN"));
		cb.closeconnection();
		return e;
	}
	public ArrayList<Chambre> chargerchambres(HttpServletRequest request){
		Chambrebd cbd=new Chambrebd();
		ArrayList<Chambre> lc=cbd.listerchambre();
		cbd.closeconnection();
		return lc;
	}
	public String supprimerchambre(HttpServletRequest request) {
		Chambrebd cb=new Chambrebd();
		String resultat=cb.delete(request.getParameter("id_chambre"));
		cb.closeconnection();
		return resultat;
	}
	public String ajouterchambre(HttpServletRequest request) {
		Chambrebd cbd=new Chambrebd();
		int n_chambre=Integer.parseInt(request.getParameter("numero_chambre"));
		String resultat= cbd.Ajouterchambre(n_chambre, request.getParameter("type_chambre"), request.getParameter("battiement"));
		cbd.closeconnection();
		return resultat;
	}
	public ArrayList<Etudiant> chargeretudiants(HttpServletRequest request){
		Etudiantbd ebd=new Etudiantbd();
		ArrayList<Etudiant> le=ebd.listerEtudiant();
		ebd.closeconnection();
		return le;
	}
	public String supprimeretudiant(HttpServletRequest request) {
		Etudiantbd eb=new Etudiantbd();
		String resultat=eb.deleteetudiant(request.getParameter("CIN"));
		eb.closeconnection();
		return resultat;
	}
	public String ajouteretudiant(HttpServletRequest request) {
		Etudiantbd cbe=new Etudiantbd();
		String resultat= cbe.AjouterEtudiant(request.getParameter("CIN"),  request.getParameter("nom"),  request.getParameter("prenom"));
		cbe.closeconnection();
		return resultat;
	}
	public ArrayList<Reclamation> chargerreclamationsv(HttpServletRequest request){
		Reclamationbd recbd=new Reclamationbd();
		ArrayList<Reclamation> lrv=recbd.listerreclamationv();
		recbd.closeconnection();
		return lrv;
	}
	public ArrayList<Reclamation> chargerreclamationsa(HttpServletRequest request){
		Reclamationbd recbd=new Reclamationbd();
		ArrayList<Reclamation> lra=recbd.listerreclamationa();
		recbd.closeconnection();
		return lra;
	}
	public ArrayList<Reclamation> chargerreclamationsn(HttpServletRequest request){
		Reclamationbd recbd=new Reclamationbd();
		ArrayList<Reclamation> lrn=recbd.listerreclamationn();
		recbd.closeconnection();
		return lrn;
	}
	public Reclamation chercherReclamation(HttpServletRequest request) {
		Reclamationbd rb=new Reclamationbd();
		int id=Integer.parseInt(request.getParameter("id_reclamation"));
		Reclamation r=rb.chercherreclamation(id);
		rb.closeconnection();
		return r;
	}
	public String validerreclamation(HttpServletRequest request) {
		Reclamationbd rb=new Reclamationbd();
		int id=Integer.parseInt(request.getParameter("id_reclamation"));
		String resultat= rb.validerreclamation(id,"VALID");
		rb.closeconnection();
		return resultat;
	}
	public String invaliderreclamation(HttpServletRequest request) {
		Reclamationbd rb=new Reclamationbd();
		int id=Integer.parseInt(request.getParameter("id_reclamation"));
		String resultat= rb.validerreclamation(id,"NON VALIDE");
		rb.closeconnection();
		return resultat;
	}
	public ArrayList<Reservation> chargerreservationsv(HttpServletRequest request){
		Reservationbd rebd=new Reservationbd();
		ArrayList<Reservation> lrv=rebd.listerreservationv();
		rebd.closeconnection();
		return lrv;
	}
	public ArrayList<Reservation> chargerreservationsa(HttpServletRequest request){
		Reservationbd rebd=new Reservationbd();
		ArrayList<Reservation> lrv=rebd.listerreservationa();
		rebd.closeconnection();
		return lrv;
	}
	public ArrayList<Reservation> chargerreservationsn(HttpServletRequest request){
		Reservationbd rebd=new Reservationbd();
		ArrayList<Reservation> lrv=rebd.listerreservationn();
		rebd.closeconnection();
		return lrv;
	}
	public Reservation chercherReservation(HttpServletRequest request) {
		Reservationbd rebd=new Reservationbd();
		int id=Integer.parseInt(request.getParameter("id_reservation"));
		Reservation r=rebd.chercherreservation(id);
		rebd.closeconnection();
		return r;
	}
	public String validerreservation(HttpServletRequest request) {
		Reservationbd rebd=new Reservationbd();
		int id=Integer.parseInt(request.getParameter("id_reservation"));
		String resultat= rebd.validerresrvation(id, "VALID");
		rebd.closeconnection();
		return resultat;
	}
	public String invaliderreservation(HttpServletRequest request) {
		Reservationbd rebd=new Reservationbd();
		int id=Integer.parseInt(request.getParameter("id_reservation"));
		String resultat= rebd.invaliderresrvation(id, "NON VALIDE");
		rebd.closeconnection();
		return resultat;
	}
	public ArrayList<Fichier> chargerfichierv(HttpServletRequest request){
		Piecebd fbd=new Piecebd();
		ArrayList<Fichier> lfv=fbd.listerfichiersv();
		fbd.closeconnection();
		return lfv;
	}
	public ArrayList<Fichier> chargerfichiera(HttpServletRequest request){
		Piecebd fbd=new Piecebd();
		ArrayList<Fichier> lfv=fbd.listerfichiersa();
		fbd.closeconnection();
		return lfv;
	}
	public ArrayList<Fichier> chargerfichiern(HttpServletRequest request){
		Piecebd fbd=new Piecebd();
		ArrayList<Fichier> lfv=fbd.listerfichiersn();
		fbd.closeconnection();
		return lfv;
	}
	public Fichier chercherFichier(HttpServletRequest request) {
		Piecebd pbd=new Piecebd();
		Fichier f=pbd.chercherfichier(request.getParameter("id_fichier"));
		pbd.closeconnection();
		return f;
	}
	public String validerfichier(HttpServletRequest request) {
		Piecebd pbd=new Piecebd();
		String resultat= pbd.validerFichier(request.getParameter("id_fichier"), "VALID");
		pbd.closeconnection();
		return resultat;
	}
	public String invaliderfichier(HttpServletRequest request) {
		Piecebd pbd=new Piecebd();
		String resultat= pbd.validerFichier(request.getParameter("id_fichier"), "NON VALIDE");
		pbd.closeconnection();
		return resultat;
	}

	public ArrayList<payement> chargerpayementv(HttpServletRequest request){
		Payementbd pbd=new Payementbd();
		ArrayList<payement> lp=pbd.listerpaymentsv();
		pbd.closeconnection();
		return lp;
	}
	public ArrayList<payement> chargerpayementa(HttpServletRequest request){
		Payementbd pbd=new Payementbd();
		ArrayList<payement> lp=pbd.listerpaymentsa();
		pbd.closeconnection();
		return lp;
	}
	public ArrayList<payement> chargerpayementn(HttpServletRequest request){
		Payementbd pbd=new Payementbd();
		ArrayList<payement> lp=pbd.listerpaymentsn();
		pbd.closeconnection();
		return lp;
	}
	public payement chercherpayement(HttpServletRequest request) {
		Payementbd pbd=new Payementbd();
		payement p=pbd.chercherpayement(request.getParameter("id_payement"));
		pbd.closeconnection();
		return p;
	}
	public String validerpayement(HttpServletRequest request) {
		Payementbd pbd=new Payementbd();
		String resultat= pbd.validerpayement(request.getParameter("id_payement"), "VALID");
		pbd.closeconnection();
		return resultat;
	}
	public String invaliderpayement(HttpServletRequest request) {
		Payementbd pbd=new Payementbd();
		String resultat= pbd.invaliderpayement(request.getParameter("id_payement"), "NON VALIDE");
		pbd.closeconnection();
		return resultat;
	}
}
