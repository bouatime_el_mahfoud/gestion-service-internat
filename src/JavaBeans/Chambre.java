package JavaBeans;

public class Chambre {
	private String id_chambre;
	private String type;
	private String battiement;
	private String etat;
	private int n_chambre;
	public String getId_chambre() {
		return id_chambre;
	}
	public void setId_chambre(String id_chambre) {
		this.id_chambre = id_chambre;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getBattiement() {
		return battiement;
	}
	public void setBattiement(String battiement) {
		this.battiement = battiement;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public int getN_chambre() {
		return n_chambre;
	}
	public void setN_chambre(int n_chambre) {
		this.n_chambre = n_chambre;
	}
	
}
