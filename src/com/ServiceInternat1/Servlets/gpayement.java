package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Fichier;
import JavaBeans.payement;

/**
 * Servlet implementation class gpayement
 */
@WebServlet("/gpayement")
public class gpayement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public gpayement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 HttpSession session=request.getSession();
		 Administrateur a=new Administrateur();
		 if(a.connexion(request)) {
			 ArrayList<payement> liste1=a.chargerpayementv(request);
			 ArrayList<payement> liste2=a.chargerpayementa(request);
			 ArrayList<payement> liste3=a.chargerpayementn(request);
			 request.setAttribute("liste1", liste1);
			 request.setAttribute("liste2", liste2);
			 request.setAttribute("liste3", liste3);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gpayements.jsp").forward(request, response);
			
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connetion.jsp").forward(request, response);
		}
		 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Administrateur a=new Administrateur();
		 Fichier f=new Fichier();
		 ArrayList<payement> liste1;
		 ArrayList<payement> liste2;
		 ArrayList<payement> liste3;
		if(request.getParameter("chercher")!=null) {
			
			payement p=a.chercherpayement(request);
			if(f.getId_fichier()!=null) {

				request.setAttribute("payement",p);
				liste1=a.chargerpayementv(request);
				liste2=a.chargerpayementa(request);
				liste3=a.chargerpayementn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gpayements.jsp").forward(request, response);
			}
			else {
				request.setAttribute("payement",p);
				liste1=a.chargerpayementv(request);
				liste2=a.chargerpayementa(request);
				liste3=a.chargerpayementn(request);
				request.setAttribute("liste1", liste1);
				request.setAttribute("liste2", liste2);
				request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gpayements.jsp").forward(request, response);
				
			}
			
		}
		if(request.getParameter("valider")!=null) {
			 String resultat=a.validerpayement(request);
			 request.setAttribute("resultat", resultat);
			 liste1=a.chargerpayementv(request);
			 liste2=a.chargerpayementa(request);
			 liste3=a.chargerpayementn(request);
			 request.setAttribute("liste1", liste1);
			 request.setAttribute("liste2", liste2);
			 request.setAttribute("liste3", liste3);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gpayements.jsp").forward(request, response);
		}
		if(request.getParameter("nvalider")!=null) {
			 String resultat=a.invaliderpayement(request);
			 	request.setAttribute("resultat", resultat);
			 	liste1=a.chargerpayementv(request);
				liste2=a.chargerpayementa(request);
				liste3=a.chargerpayementn(request);
				request.setAttribute("liste1", liste1);
				request.setAttribute("liste2", liste2);
				request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gpayements.jsp").forward(request, response);
		}
	}

}
