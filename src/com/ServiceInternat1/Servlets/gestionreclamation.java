package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Chambre;
import JavaBeans.Reclamation;

/**
 * Servlet implementation class gestionreclamation
 */
@WebServlet("/gestionreclamation")
public class gestionreclamation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public gestionreclamation() {
        super();
        
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession Session=request.getSession();
		 Administrateur a=new Administrateur();
		 if(a.connexion(request)) {
				
			 ArrayList<Reclamation> liste1=a.chargerreclamationsv(request);
			 ArrayList<Reclamation> liste2=a.chargerreclamationsa(request);
			 ArrayList<Reclamation> liste3=a.chargerreclamationsn(request);
			 request.setAttribute("liste1", liste1);
			 request.setAttribute("liste2", liste2);
			 request.setAttribute("liste3", liste3);
			this.getServletContext().getRequestDispatcher("/WEB-INF/greclamations.jsp").forward(request, response);
			}
		 else {
			 this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		 }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 Administrateur a=new Administrateur();
		 Reclamation r=new Reclamation();
		 ArrayList<Reclamation> liste1;
		 ArrayList<Reclamation> liste2;
		 ArrayList<Reclamation> liste3;
		if(request.getParameter("chercher")!=null) {
			
			r=a.chercherReclamation(request);
			if(r.getId_reclamation()!=0) {

				request.setAttribute("reclamation",r);
				liste1=a.chargerreclamationsv(request);
				liste2=a.chargerreclamationsa(request);
				liste3=a.chargerreclamationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greclamations.jsp").forward(request, response);
			}
			else {
				request.setAttribute("reclamation",r);
				liste1=a.chargerreclamationsv(request);
				liste2=a.chargerreclamationsa(request);
				liste3=a.chargerreclamationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greclamations.jsp").forward(request, response);
				
			}
			
		}
		if(request.getParameter("valider")!=null) {
			 String resultat=a.validerreclamation(request);
			 request.setAttribute("resultat", resultat);
				liste1=a.chargerreclamationsv(request);
				liste2=a.chargerreclamationsa(request);
				liste3=a.chargerreclamationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greclamations.jsp").forward(request, response);
		}
		if(request.getParameter("nvalider")!=null) {
			 String resultat=a.invaliderreclamation(request);
			 request.setAttribute("resultat", resultat);
				liste1=a.chargerreclamationsv(request);
				liste2=a.chargerreclamationsa(request);
				liste3=a.chargerreclamationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greclamations.jsp").forward(request, response);
		}
		
	}

}
