package com.ServiceInternat1.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Etudiant;

/**
 * Servlet implementation class Reclamation
 */
@WebServlet("/Reclamation")
public class Reclamation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Reclamation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Etudiant e=new Etudiant();
		if(e.connexion(request)) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/reclamation.jsp").forward(request, response);
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Etudiant e=new Etudiant();
		if(e.connexion(request)) {
			if(request.getParameter("reclamer")!=null) {
				String resultat=e.effectuerreclamation(request);
				request.setAttribute("message", resultat);
				this.getServletContext().getRequestDispatcher("/WEB-INF/reclamation.jsp").forward(request, response);
			}
			else {
				this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
			}
		}
		
	}

}
