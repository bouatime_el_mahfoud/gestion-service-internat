package com.ServiceInternat1.Servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import BaseDonn�e.Payementbd;
import JavaBeans.payement;



/**
 * Servlet implementation class RecuPdf
 */
@WebServlet("/RecuPdf")
public class RecuPdf extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RecuPdf() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session =request.getSession();
		Payementbd P=new Payementbd();
		String message="impossible d'imprimer le re�u d'inscription avant la validation du payement";
		
		payement py=P.chercherpayementcin((String) session.getAttribute("CIN"));
		if(py!=null) {
			if(py.getEtat().equals("VALID")) {
				makePdf(request,response,(String)session.getAttribute("nom"),(String)session.getAttribute("prenom"),(String)session.getAttribute("CIN"),(int)session.getAttribute("numero_chambre"),(String) session.getAttribute("battiement"),(String)py.getMode(),(String)py.getTranche(),(String)py.getId_payement(),(String)session.getAttribute("type_chambre"));
			}
			else {
				request.setAttribute("message", message);
				this.getServletContext().getRequestDispatcher("/WEB-INF/Payement.jsp").forward(request, response);
			}
		}
		else {
			request.setAttribute("message", message);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Payement.jsp").forward(request, response);
		}
			
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	public void makePdf(HttpServletRequest request, HttpServletResponse response,String nom,String prenom,String cin,int n_chambre,String battiement,String mode_payement,String tranche,String id_payement,String type_chambre){
		try {
			
			Document document = new Document();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, baos);
			document.addAuthor("ServiceInternat");
			document.addTitle("Re�u_Service_Internat_bouatim-elemahfoud");
			document.open();
			Image logo=Image.getInstance("C:/Users/theblackme/eclipse-workspace/ServiceInternat1/WebContent/img/logo.jpg");
			 logo.scaleAbsolute(100f, 100f);
			document.add(logo);
			Paragraph p = new Paragraph("Re�u d'inscription au service internat", new
					Font(Font.TIMES_ROMAN , 25,Font.UNDERLINE) );
			
			p.setAlignment("CENTER");
			document.add(p);
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         NOM:    "+nom, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         PRENOM:    "+prenom, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         CIN:    "+cin, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         TYPE_CHAMBRE:    "+type_chambre, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         N_CHAMBRE:    "+n_chambre, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         BATTIEMENT:    "+battiement, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         ID_Payement:    "+id_payement, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         MODE_PAYEMENT:    "+mode_payement, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("         TYPE_PAYEMENT:    "+tranche, new
					Font(Font.TIMES_ROMAN , 18,Font.BOLDITALIC) ));
			document.close();

			
			response.setHeader("Expires", "0");
			response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
			response.setHeader("Pragma", "public");

			response.setContentType("application/pdf");

			response.setContentLength(baos.size());

			ServletOutputStream out = response.getOutputStream();
			baos.writeTo(out);
			out.flush();

	    } catch (Exception e2) {
	      System.out.println("Error in " + getClass().getName() + "\n" + e2);
	    }
		    
		  }

}
