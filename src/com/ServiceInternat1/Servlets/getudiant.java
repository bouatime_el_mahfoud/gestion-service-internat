package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Etudiant;

/**
 * Servlet implementation class getudiant
 */
@WebServlet("/getudiant")
public class getudiant extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public getudiant() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Administrateur a=new Administrateur();
		if(a.connexion(request)) {

			ArrayList<Etudiant> le=a.chargeretudiants(request);
			request.setAttribute("liste", le);
			this.getServletContext().getRequestDispatcher("/WEB-INF/getudiants.jsp").forward(request, response);
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Administrateur a=new Administrateur();
		Etudiant e;
		ArrayList<Etudiant> le;
		if(request.getParameter("supprimer")!=null) {
			String res=a.supprimeretudiant(request);
			request.setAttribute("resultat", res);
			le=a.chargeretudiants(request);
			request.setAttribute("liste", le);
			this.getServletContext().getRequestDispatcher("/WEB-INF/getudiants.jsp").forward(request, response);
		}
		if(request.getParameter("chercher")!=null) {
			e=a.chercherEtudiant(request);
			if(e.getCIN()!=null) {

				request.setAttribute("etudiant",e);
				le=a.chargeretudiants(request);
				request.setAttribute("liste", le);
				this.getServletContext().getRequestDispatcher("/WEB-INF/getudiants.jsp").forward(request, response);
			}
			else {
				request.setAttribute("etudiant",e);
				le=a.chargeretudiants(request);
				request.setAttribute("liste", le);
				this.getServletContext().getRequestDispatcher("/WEB-INF/getudiants.jsp").forward(request, response);
				
			}
		}
		if(request.getParameter("ajouter")!=null) {
			String res=a.ajouteretudiant(request);
			request.setAttribute("resultat1", res);
			le=a.chargeretudiants(request);
			request.setAttribute("liste", le);
			this.getServletContext().getRequestDispatcher("/WEB-INF/getudiants.jsp").forward(request, response);
			
			
		}
	}

}
