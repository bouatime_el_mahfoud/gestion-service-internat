package com.ServiceInternat1.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Etudiant;

/**
 * Servlet implementation class Payement
 */
@WebServlet("/Payement")
public class Payement extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Payement() {
        super();
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession seesion=request.getSession();
		Etudiant e=new Etudiant() ;
			if(e.connexion(request)) {
				String message=e.messagepayement(request);
				request.setAttribute("message", message);
				this.getServletContext().getRequestDispatcher("/WEB-INF/Payement.jsp").forward(request, response);
		}
			else {
				this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession seesion=request.getSession();
		Etudiant e=new Etudiant() ;
		if(e.connexion(request)) {
			if(request.getParameter("deposer")!=null) {
				String message1=e.effectuerpayement(request);
				request.setAttribute("message1", message1);
				String message=e.messagepayement(request);
				request.setAttribute("message", message);
				
				this.getServletContext().getRequestDispatcher("/WEB-INF/Payement.jsp").forward(request, response);
			}
	}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
		}
	}

}
