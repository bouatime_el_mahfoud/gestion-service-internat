package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Etudiant;
import JavaBeans.Fichier;
import JavaBeans.Reclamation;

/**
 * Servlet implementation class gfichier
 */
@WebServlet("/gfichier")
public class gfichier extends HttpServlet {
	private static final long serialVersionUID = 1L;
     
    public gfichier() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Administrateur a=new Administrateur();
		if(a.connexion(request)) {
			 ArrayList<Fichier> liste1=a.chargerfichierv(request);
			 ArrayList<Fichier> liste2=a.chargerfichiera(request);
			 ArrayList<Fichier> liste3=a.chargerfichiern(request);
			 request.setAttribute("liste1", liste1);
			 request.setAttribute("liste2", liste2);
			 request.setAttribute("liste3", liste3);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gfichiers.jsp").forward(request, response);
			
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Administrateur a=new Administrateur();
		 Fichier f=new Fichier();
		 ArrayList<Fichier> liste1;
		 ArrayList<Fichier> liste2;
		 ArrayList<Fichier> liste3;
		if(request.getParameter("chercher")!=null) {
			
			f=a.chercherFichier(request);
			if(f.getId_fichier()!=null) {

				request.setAttribute("Fichier",f);
				liste1=a.chargerfichierv(request);
				liste2=a.chargerfichiera(request);
				liste3=a.chargerfichiern(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gfichiers.jsp").forward(request, response);
			}
			else {
				request.setAttribute("Fichier",f);
				liste1=a.chargerfichierv(request);
				liste2=a.chargerfichiera(request);
				liste3=a.chargerfichiern(request);
				request.setAttribute("liste1", liste1);
				request.setAttribute("liste2", liste2);
				request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gfichiers.jsp").forward(request, response);
				
			}
			
		}
		if(request.getParameter("valider")!=null) {
			 String resultat=a.validerfichier(request);
			 request.setAttribute("resultat", resultat);
			 	liste1=a.chargerfichierv(request);
				liste2=a.chargerfichiera(request);
				liste3=a.chargerfichiern(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gfichiers.jsp").forward(request, response);
		}
		if(request.getParameter("nvalider")!=null) {
			 String resultat=a.invaliderfichier(request);
			 request.setAttribute("resultat", resultat);
			 request.setAttribute("resultat", resultat);
			 	liste1=a.chargerfichierv(request);
				liste2=a.chargerfichiera(request);
				liste3=a.chargerfichiern(request);
				request.setAttribute("liste1", liste1);
				request.setAttribute("liste2", liste2);
				request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gfichiers.jsp").forward(request, response);
		}

	}

}
