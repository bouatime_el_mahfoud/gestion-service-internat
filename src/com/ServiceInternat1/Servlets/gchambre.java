package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Chambre;

/**
 * Servlet implementation class gchambre
 */
@WebServlet("/gchambre")
public class gchambre extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public gchambre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Administrateur a=new Administrateur();
		if(a.connexion(request)) {
			ArrayList<Chambre> lc=a.chargerchambres(request);
			request.setAttribute("liste", lc);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gchambre.jsp").forward(request, response);
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Chambre c=new Chambre();
		Administrateur a =new Administrateur();
		ArrayList<Chambre> lc;
		if(request.getParameter("supprimer")!=null) {
			String res=a.supprimerchambre(request);
			request.setAttribute("resultat", res);
			lc=a.chargerchambres(request);
			request.setAttribute("liste", lc);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gchambre.jsp").forward(request, response);
		}
		if(request.getParameter("ajouter")!=null) {
			String res=a.ajouterchambre(request);
			request.setAttribute("resultat1", res);
			lc=a.chargerchambres(request);
			request.setAttribute("liste", lc);
			this.getServletContext().getRequestDispatcher("/WEB-INF/gchambre.jsp").forward(request, response);
			
			
		}
		if(request.getParameter("chercher")!=null) {
			System.out.println("hi");
			c=a.chercherchambre1(request);
			if(c.getId_chambre()!=null) {

				request.setAttribute("chambre",c);
				lc=a.chargerchambres(request);
				request.setAttribute("liste", lc);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gchambre.jsp").forward(request, response);
			}
			else {
				request.setAttribute("chambre",c);
				lc=a.chargerchambres(request);
				request.setAttribute("liste", lc);
				this.getServletContext().getRequestDispatcher("/WEB-INF/gchambre.jsp").forward(request, response);
				
			}
		}
		
	}

}
