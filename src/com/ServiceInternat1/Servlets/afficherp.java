package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;

import JavaBeans.Administrateur;

/**
 * Servlet implementation class afficherp
 */
@WebServlet("/afficherp")
public class afficherp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public afficherp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Connection conn=null;
			ResultSet rs=null;
			PreparedStatement ps=null;
			OutputStream img;
				try {
					conn=  (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ServiceInternat1","root","root1234");
					ps=conn.prepareStatement("select pay from payement where id_payement=?");
					ps.setString(1,request.getParameter("id_payement"));
					rs=ps.executeQuery();
					if(rs.next()) {
						 byte barray[] = rs.getBytes(1);
					        response.setContentType("application/pdf");
					        img=response.getOutputStream();
					        img.write(barray);
					        img.flush();
					        img.close();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
    	
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
