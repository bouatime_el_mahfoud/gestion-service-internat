package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import BaseDonnée.Reservationbd;
import JavaBeans.Etudiant;


/**
 * Servlet implementation class Reservation
 */
@WebServlet("/Reservation")
public class Reservation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Reservation() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session =request.getSession();
		Etudiant e=new Etudiant();
		if(e.connexion(request)) {
			String res=e.reserverchambre(request);
			request.setAttribute("messagereserver",res);
			this.getServletContext().getRequestDispatcher("/WEB-INF/reservation.jsp").forward(request, response);
		}
		else{
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Etudiant e=new Etudiant();
		if(request.getParameter("chercher")!=null) {
		
			ArrayList<String> lc=e.chercherreserve(request);
			String res=e.reserverchambre(request);
			request.setAttribute("messagereserver",res);
			request.setAttribute("liste", lc);
			this.getServletContext().getRequestDispatcher("/WEB-INF/reservation.jsp").forward(request,response);
		}
		else if(request.getParameter("reserver")!=null) {
			String res1=e.reservationchambre(request);
			request.setAttribute("message", res1);
			String res=e.reserverchambre(request);
			request.setAttribute("messagereserver",res);
			this.getServletContext().getRequestDispatcher("/WEB-INF/reservation.jsp").forward(request,response);
		}
	}

}
