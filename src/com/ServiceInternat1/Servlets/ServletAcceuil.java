package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import BaseDonnée.Chambrebd;
import BaseDonnée.Etudiantbd;
import BaseDonnée.Payementbd;
import BaseDonnée.Piecebd;
import BaseDonnée.Reclamationbd;
import BaseDonnée.Reservationbd;
import JavaBeans.Administrateur;
import JavaBeans.Chambre;
import JavaBeans.Etudiant;
import JavaBeans.Fichier;




public class ServletAcceuil extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public ServletAcceuil() {
        super();
      
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		BaseDonnée.Administrateur a1=new BaseDonnée.Administrateur();
		Chambrebd c1=new Chambrebd();
		Etudiantbd e1=new Etudiantbd();
		Reservationbd re=new Reservationbd();
		Payementbd pb=new Payementbd();
		Piecebd pbb=new Piecebd();
		Reclamationbd rbd=new Reclamationbd();
		HttpSession session=request.getSession();
		this.getServletContext().getRequestDispatcher("/WEB-INF/index1.jsp").forward(request, response);
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
	}

}
