package com.ServiceInternat1.Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Reclamation;
import JavaBeans.Reservation;

/**
 * Servlet implementation class greservations
 */
@WebServlet("/greservations")
public class greservations extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public greservations() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession Session=request.getSession();
		Administrateur a=new Administrateur();
		 if(a.connexion(request)) {
			 ArrayList<Reservation> liste1=a.chargerreservationsv(request);
			 ArrayList<Reservation> liste2=a.chargerreservationsa(request);
			 ArrayList<Reservation> liste3=a.chargerreservationsn(request);
			 request.setAttribute("liste1", liste1);
			 request.setAttribute("liste2", liste2);
			 request.setAttribute("liste3", liste3);
			this.getServletContext().getRequestDispatcher("/WEB-INF/greservation.jsp").forward(request, response);
			}
		 else {
			 this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		 }
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Administrateur a=new Administrateur();
		 Reservation r=new Reservation();
		 ArrayList<Reservation> liste1;
		 ArrayList<Reservation> liste2;
		 ArrayList<Reservation> liste3;
		if(request.getParameter("chercher")!=null) {
			
			r=a.chercherReservation(request);
			if(r.getId_reservation()!=0) {

				request.setAttribute("reservation",r);
				liste1=a.chargerreservationsv(request);
				liste2=a.chargerreservationsa(request);
				liste3=a.chargerreservationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greservation.jsp").forward(request, response);
			}
			else {
				request.setAttribute("reservation",r);
				liste1=a.chargerreservationsv(request);
				liste2=a.chargerreservationsa(request);
				liste3=a.chargerreservationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greservation.jsp").forward(request, response);
				
			}
			
		}
		if(request.getParameter("valider")!=null) {
			 String resultat=a.validerreservation(request);
			 request.setAttribute("resultat", resultat);
			 	liste1=a.chargerreservationsv(request);
				liste2=a.chargerreservationsa(request);
				liste3=a.chargerreservationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greservation.jsp").forward(request, response);
		}
		if(request.getParameter("nvalider")!=null) {
			 String resultat=a.invaliderreservation(request);
			 request.setAttribute("resultat", resultat);
			 liste1=a.chargerreservationsv(request);
				liste2=a.chargerreservationsa(request);
				liste3=a.chargerreservationsn(request);
				request.setAttribute("liste1", liste1);
				 request.setAttribute("liste2", liste2);
				 request.setAttribute("liste3", liste3);
				this.getServletContext().getRequestDispatcher("/WEB-INF/greservation.jsp").forward(request, response);
		}
		
	}
	

}
