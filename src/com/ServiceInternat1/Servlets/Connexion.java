package com.ServiceInternat1.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Etudiant;

/**
 * Servlet implementation class Connexion
 */
@WebServlet("/Connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Connexion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session=request.getSession();
		session.setAttribute("login", request.getParameter("login"));
		session.setAttribute("password", request.getParameter("password"));
		Administrateur a=new Administrateur();
		Etudiant e=new Etudiant();
		if(a.connexion(request)) {
			
			this.getServletContext().getRequestDispatcher("/WEB-INF/ad.jsp").forward(request, response);
			
		}
		else if(e.connexion(request)) {
			e=e.chargeretudiant(request);
			session.setAttribute("CIN", e.getCIN());
			session.setAttribute("nom", e.getNom());
			session.setAttribute("prenom", e.getPrenom());
			session.setAttribute("numero_chambre", e.getNumero_chambre());
			session.setAttribute("sexe", e.getSexe());
			session.setAttribute("battiement", e.getBattiement());
			System.out.println(e.getBattiement());
			session.setAttribute("type_chambre", e.getType_chambre());
			request.setAttribute("etudiant", e);
			this.getServletContext().getRequestDispatcher("/WEB-INF/Menuetudiant.jsp").forward(request, response);
			
		}
		else {

			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
	}

}
