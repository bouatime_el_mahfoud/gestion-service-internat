package com.ServiceInternat1.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Administrateur;
import JavaBeans.Chambre;
import JavaBeans.Etudiant;


@WebServlet("/MenuAdmin")
public class MenuAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public MenuAdmin() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Administrateur a=new Administrateur();
		if(a.connexion(request)) {
			this.getServletContext().getRequestDispatcher("/WEB-INF/ad.jsp").forward(request, response);
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		Etudiant e=new Etudiant();
		Chambre c=new Chambre();
		Administrateur a=new Administrateur();
		if((request.getParameter("cherchec"))!=null){
			c=a.chercherchambre(request);
			if(c.getId_chambre()!=null) {
				
				request.setAttribute("chambre",c);
				this.getServletContext().getRequestDispatcher("/WEB-INF/ad.jsp").forward(request, response);
			}
			else {
				request.setAttribute("chambre",c);
				this.getServletContext().getRequestDispatcher("/WEB-INF/ad.jsp").forward(request, response);
				
			}
		}
		if((request.getParameter("cherchere"))!=null){
			e=a.chercherEtudiant(request);

			if(e.getCIN()!=null) {
				
				request.setAttribute("etudiant",e);
				this.getServletContext().getRequestDispatcher("/WEB-INF/ad.jsp").forward(request, response);
			}
			else {
				request.setAttribute("etudiant",e);
				this.getServletContext().getRequestDispatcher("/WEB-INF/ad.jsp").forward(request, response);
				
			}
		}
		
	}

}
