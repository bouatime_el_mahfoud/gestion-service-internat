package com.ServiceInternat1.Servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import JavaBeans.Etudiant;
import JavaBeans.Fichier;

/**
 * Servlet implementation class Depot
 */
@WebServlet("/Depot")
public class Depot extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Depot() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session=request.getSession();
		Fichier f=new Fichier();
		Etudiant e=new Etudiant();
		if(e.connexion(request)) {
			String messagedepot=f.messagedepot(request);
			System.out.println(messagedepot);
			request.setAttribute("messagedepot", messagedepot);
			this.getServletContext().getRequestDispatcher("/WEB-INF/depot.jsp").forward(request, response);
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Etudiant e=new Etudiant();
		Fichier f=new Fichier();
		if(e.connexion(request)) {
			if(request.getParameter("deposer")!=null) {
				String message=f.deposerfichier(request);
				String messagedepot=f.messagedepot(request);
				request.setAttribute("message", message);
				request.setAttribute("messagedepot", messagedepot);
				this.getServletContext().getRequestDispatcher("/WEB-INF/depot.jsp").forward(request, response);
			}
		}
		else {
			this.getServletContext().getRequestDispatcher("/WEB-INF/connection.jsp").forward(request, response);
		}
	}

}
